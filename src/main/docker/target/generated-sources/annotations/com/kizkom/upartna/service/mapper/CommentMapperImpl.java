package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.Comment;
import com.kizkom.upartna.service.dto.CommentDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-10-21T17:56:50+0200",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_201 (Oracle Corporation)"
)
@Component
public class CommentMapperImpl implements CommentMapper {

    @Override
    public Comment toEntity(CommentDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Comment comment = new Comment();

        comment.setId( dto.getId() );
        comment.setMessage( dto.getMessage() );
        comment.setCreatedOn( dto.getCreatedOn() );
        comment.setUser( dto.getUser() );
        comment.setProof( dto.getProof() );

        return comment;
    }

    @Override
    public CommentDTO toDto(Comment entity) {
        if ( entity == null ) {
            return null;
        }

        CommentDTO commentDTO = new CommentDTO();

        commentDTO.setId( entity.getId() );
        commentDTO.setMessage( entity.getMessage() );
        commentDTO.setCreatedOn( entity.getCreatedOn() );
        commentDTO.setUser( entity.getUser() );
        commentDTO.setProof( entity.getProof() );

        return commentDTO;
    }

    @Override
    public List<Comment> toEntity(List<CommentDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Comment> list = new ArrayList<Comment>( dtoList.size() );
        for ( CommentDTO commentDTO : dtoList ) {
            list.add( toEntity( commentDTO ) );
        }

        return list;
    }

    @Override
    public List<CommentDTO> toDto(List<Comment> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<CommentDTO> list = new ArrayList<CommentDTO>( entityList.size() );
        for ( Comment comment : entityList ) {
            list.add( toDto( comment ) );
        }

        return list;
    }
}
