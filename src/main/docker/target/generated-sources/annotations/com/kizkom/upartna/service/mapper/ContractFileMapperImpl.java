package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.ContractFile;
import com.kizkom.upartna.service.dto.ContractFileDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-10-21T17:56:50+0200",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_201 (Oracle Corporation)"
)
@Component
public class ContractFileMapperImpl implements ContractFileMapper {

    @Override
    public ContractFile toEntity(ContractFileDTO dto) {
        if ( dto == null ) {
            return null;
        }

        ContractFile contractFile = new ContractFile();

        contractFile.setId( dto.getId() );
        contractFile.setUrl( dto.getUrl() );
        contractFile.setFileType( dto.getFileType() );
        contractFile.setExtension( dto.getExtension() );
        contractFile.setContract( dto.getContract() );

        return contractFile;
    }

    @Override
    public ContractFileDTO toDto(ContractFile entity) {
        if ( entity == null ) {
            return null;
        }

        ContractFileDTO contractFileDTO = new ContractFileDTO();

        contractFileDTO.setId( entity.getId() );
        contractFileDTO.setUrl( entity.getUrl() );
        contractFileDTO.setFileType( entity.getFileType() );
        contractFileDTO.setExtension( entity.getExtension() );
        contractFileDTO.setContract( entity.getContract() );

        return contractFileDTO;
    }

    @Override
    public List<ContractFile> toEntity(List<ContractFileDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<ContractFile> list = new ArrayList<ContractFile>( dtoList.size() );
        for ( ContractFileDTO contractFileDTO : dtoList ) {
            list.add( toEntity( contractFileDTO ) );
        }

        return list;
    }

    @Override
    public List<ContractFileDTO> toDto(List<ContractFile> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<ContractFileDTO> list = new ArrayList<ContractFileDTO>( entityList.size() );
        for ( ContractFile contractFile : entityList ) {
            list.add( toDto( contractFile ) );
        }

        return list;
    }
}
