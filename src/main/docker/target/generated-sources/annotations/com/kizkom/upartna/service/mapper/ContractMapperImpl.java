package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.Contract;
import com.kizkom.upartna.service.dto.ContractDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-10-21T17:56:50+0200",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_201 (Oracle Corporation)"
)
@Component
public class ContractMapperImpl implements ContractMapper {

    @Override
    public Contract toEntity(ContractDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Contract contract = new Contract();

        contract.setId( dto.getId() );
        contract.setTitle( dto.getTitle() );
        contract.setCode( dto.getCode() );
        contract.setBeginningDate( dto.getBeginningDate() );
        contract.setEndingDate( dto.getEndingDate() );
        contract.setAmount( dto.getAmount() );
        contract.setSignatureDate( dto.getSignatureDate() );

        return contract;
    }

    @Override
    public ContractDTO toDto(Contract entity) {
        if ( entity == null ) {
            return null;
        }

        ContractDTO contractDTO = new ContractDTO();

        contractDTO.setId( entity.getId() );
        contractDTO.setTitle( entity.getTitle() );
        contractDTO.setCode( entity.getCode() );
        contractDTO.setBeginningDate( entity.getBeginningDate() );
        contractDTO.setEndingDate( entity.getEndingDate() );
        contractDTO.setAmount( entity.getAmount() );
        contractDTO.setSignatureDate( entity.getSignatureDate() );

        return contractDTO;
    }

    @Override
    public List<Contract> toEntity(List<ContractDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Contract> list = new ArrayList<Contract>( dtoList.size() );
        for ( ContractDTO contractDTO : dtoList ) {
            list.add( toEntity( contractDTO ) );
        }

        return list;
    }

    @Override
    public List<ContractDTO> toDto(List<Contract> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<ContractDTO> list = new ArrayList<ContractDTO>( entityList.size() );
        for ( Contract contract : entityList ) {
            list.add( toDto( contract ) );
        }

        return list;
    }
}
