package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.Conversation;
import com.kizkom.upartna.service.dto.ConversationDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-10-21T17:56:50+0200",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_201 (Oracle Corporation)"
)
@Component
public class ConversationMapperImpl implements ConversationMapper {

    @Override
    public Conversation toEntity(ConversationDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Conversation conversation = new Conversation();

        conversation.setId( dto.getId() );
        conversation.setCreatedOn( dto.getCreatedOn() );
        conversation.setClosedOn( dto.getClosedOn() );

        return conversation;
    }

    @Override
    public ConversationDTO toDto(Conversation entity) {
        if ( entity == null ) {
            return null;
        }

        ConversationDTO conversationDTO = new ConversationDTO();

        conversationDTO.setId( entity.getId() );
        conversationDTO.setCreatedOn( entity.getCreatedOn() );
        conversationDTO.setClosedOn( entity.getClosedOn() );

        return conversationDTO;
    }

    @Override
    public List<Conversation> toEntity(List<ConversationDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Conversation> list = new ArrayList<Conversation>( dtoList.size() );
        for ( ConversationDTO conversationDTO : dtoList ) {
            list.add( toEntity( conversationDTO ) );
        }

        return list;
    }

    @Override
    public List<ConversationDTO> toDto(List<Conversation> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<ConversationDTO> list = new ArrayList<ConversationDTO>( entityList.size() );
        for ( Conversation conversation : entityList ) {
            list.add( toDto( conversation ) );
        }

        return list;
    }
}
