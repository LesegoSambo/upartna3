package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.Country;
import com.kizkom.upartna.service.dto.CountryDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-10-21T17:56:50+0200",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_201 (Oracle Corporation)"
)
@Component
public class CountryMapperImpl implements CountryMapper {

    @Override
    public Country toEntity(CountryDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Country country = new Country();

        country.setId( dto.getId() );
        country.setName( dto.getName() );
        country.setPhoneNumberCode( dto.getPhoneNumberCode() );

        return country;
    }

    @Override
    public CountryDTO toDto(Country entity) {
        if ( entity == null ) {
            return null;
        }

        CountryDTO countryDTO = new CountryDTO();

        countryDTO.setId( entity.getId() );
        countryDTO.setName( entity.getName() );
        countryDTO.setPhoneNumberCode( entity.getPhoneNumberCode() );

        return countryDTO;
    }

    @Override
    public List<Country> toEntity(List<CountryDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Country> list = new ArrayList<Country>( dtoList.size() );
        for ( CountryDTO countryDTO : dtoList ) {
            list.add( toEntity( countryDTO ) );
        }

        return list;
    }

    @Override
    public List<CountryDTO> toDto(List<Country> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<CountryDTO> list = new ArrayList<CountryDTO>( entityList.size() );
        for ( Country country : entityList ) {
            list.add( toDto( country ) );
        }

        return list;
    }
}
