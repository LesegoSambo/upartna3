package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.Domain;
import com.kizkom.upartna.service.dto.DomainDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-10-21T17:56:50+0200",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_201 (Oracle Corporation)"
)
@Component
public class DomainMapperImpl implements DomainMapper {

    @Override
    public Domain toEntity(DomainDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Domain domain = new Domain();

        domain.setId( dto.getId() );
        domain.setTitle( dto.getTitle() );
        domain.setDescription( dto.getDescription() );
        domain.setSpeciality( dto.getSpeciality() );
        domain.setParent( dto.getParent() );

        return domain;
    }

    @Override
    public DomainDTO toDto(Domain entity) {
        if ( entity == null ) {
            return null;
        }

        DomainDTO domainDTO = new DomainDTO();

        domainDTO.setId( entity.getId() );
        domainDTO.setTitle( entity.getTitle() );
        domainDTO.setDescription( entity.getDescription() );
        domainDTO.setSpeciality( entity.getSpeciality() );
        domainDTO.setParent( entity.getParent() );

        return domainDTO;
    }

    @Override
    public List<Domain> toEntity(List<DomainDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Domain> list = new ArrayList<Domain>( dtoList.size() );
        for ( DomainDTO domainDTO : dtoList ) {
            list.add( toEntity( domainDTO ) );
        }

        return list;
    }

    @Override
    public List<DomainDTO> toDto(List<Domain> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<DomainDTO> list = new ArrayList<DomainDTO>( entityList.size() );
        for ( Domain domain : entityList ) {
            list.add( toDto( domain ) );
        }

        return list;
    }
}
