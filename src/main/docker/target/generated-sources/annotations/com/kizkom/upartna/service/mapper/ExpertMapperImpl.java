package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.Expert;
import com.kizkom.upartna.service.dto.ExpertDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-10-21T17:56:50+0200",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_201 (Oracle Corporation)"
)
@Component
public class ExpertMapperImpl implements ExpertMapper {

    @Override
    public Expert toEntity(ExpertDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Expert expert = new Expert();

        expert.setId( dto.getId() );
        expert.setTitle( dto.getTitle() );
        expert.setAbout( dto.getAbout() );
        expert.setWebsite( dto.getWebsite() );
        expert.setIsActivated( dto.isIsActivated() );
        expert.setIsCompany( dto.isIsCompany() );
        expert.setTaxPayerNumber( dto.getTaxPayerNumber() );
        expert.setExperienceStartDate( dto.getExperienceStartDate() );
        expert.setActivatedOn( dto.getActivatedOn() );
        expert.setCreatedOn( dto.getCreatedOn() );
        expert.setUser( dto.getUser() );

        return expert;
    }

    @Override
    public ExpertDTO toDto(Expert entity) {
        if ( entity == null ) {
            return null;
        }

        ExpertDTO expertDTO = new ExpertDTO();

        expertDTO.setId( entity.getId() );
        expertDTO.setTitle( entity.getTitle() );
        expertDTO.setAbout( entity.getAbout() );
        expertDTO.setWebsite( entity.getWebsite() );
        expertDTO.setIsActivated( entity.isIsActivated() );
        expertDTO.setIsCompany( entity.isIsCompany() );
        expertDTO.setTaxPayerNumber( entity.getTaxPayerNumber() );
        expertDTO.setExperienceStartDate( entity.getExperienceStartDate() );
        expertDTO.setActivatedOn( entity.getActivatedOn() );
        expertDTO.setCreatedOn( entity.getCreatedOn() );
        expertDTO.setUser( entity.getUser() );

        return expertDTO;
    }

    @Override
    public List<Expert> toEntity(List<ExpertDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Expert> list = new ArrayList<Expert>( dtoList.size() );
        for ( ExpertDTO expertDTO : dtoList ) {
            list.add( toEntity( expertDTO ) );
        }

        return list;
    }

    @Override
    public List<ExpertDTO> toDto(List<Expert> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<ExpertDTO> list = new ArrayList<ExpertDTO>( entityList.size() );
        for ( Expert expert : entityList ) {
            list.add( toDto( expert ) );
        }

        return list;
    }
}
