package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.JobFile;
import com.kizkom.upartna.service.dto.JobFileDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-10-21T17:56:50+0200",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_201 (Oracle Corporation)"
)
@Component
public class JobFileMapperImpl implements JobFileMapper {

    @Override
    public JobFile toEntity(JobFileDTO dto) {
        if ( dto == null ) {
            return null;
        }

        JobFile jobFile = new JobFile();

        jobFile.setId( dto.getId() );
        jobFile.setUrl( dto.getUrl() );
        jobFile.setFileType( dto.getFileType() );
        jobFile.setExtension( dto.getExtension() );
        jobFile.setJob( dto.getJob() );

        return jobFile;
    }

    @Override
    public JobFileDTO toDto(JobFile entity) {
        if ( entity == null ) {
            return null;
        }

        JobFileDTO jobFileDTO = new JobFileDTO();

        jobFileDTO.setId( entity.getId() );
        jobFileDTO.setUrl( entity.getUrl() );
        jobFileDTO.setFileType( entity.getFileType() );
        jobFileDTO.setExtension( entity.getExtension() );
        jobFileDTO.setJob( entity.getJob() );

        return jobFileDTO;
    }

    @Override
    public List<JobFile> toEntity(List<JobFileDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<JobFile> list = new ArrayList<JobFile>( dtoList.size() );
        for ( JobFileDTO jobFileDTO : dtoList ) {
            list.add( toEntity( jobFileDTO ) );
        }

        return list;
    }

    @Override
    public List<JobFileDTO> toDto(List<JobFile> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<JobFileDTO> list = new ArrayList<JobFileDTO>( entityList.size() );
        for ( JobFile jobFile : entityList ) {
            list.add( toDto( jobFile ) );
        }

        return list;
    }
}
