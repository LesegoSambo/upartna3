package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.Job;
import com.kizkom.upartna.service.dto.JobDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-10-21T17:56:50+0200",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_201 (Oracle Corporation)"
)
@Component
public class JobMapperImpl implements JobMapper {

    @Override
    public Job toEntity(JobDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Job job = new Job();

        job.setId( dto.getId() );
        job.setTitle( dto.getTitle() );
        job.setCode( dto.getCode() );
        job.setDescription( dto.getDescription() );
        job.setBeginningDate( dto.getBeginningDate() );
        job.setEndingDate( dto.getEndingDate() );
        job.setAmount( dto.getAmount() );
        job.setIsDeleted( dto.isIsDeleted() );
        job.setCreatedOn( dto.getCreatedOn() );
        job.setUpdatedOn( dto.getUpdatedOn() );
        job.setDomain( dto.getDomain() );
        job.setClient( dto.getClient() );
        job.setCountry( dto.getCountry() );

        return job;
    }

    @Override
    public JobDTO toDto(Job entity) {
        if ( entity == null ) {
            return null;
        }

        JobDTO jobDTO = new JobDTO();

        jobDTO.setId( entity.getId() );
        jobDTO.setTitle( entity.getTitle() );
        jobDTO.setCode( entity.getCode() );
        jobDTO.setDescription( entity.getDescription() );
        jobDTO.setBeginningDate( entity.getBeginningDate() );
        jobDTO.setEndingDate( entity.getEndingDate() );
        jobDTO.setAmount( entity.getAmount() );
        jobDTO.setIsDeleted( entity.isIsDeleted() );
        jobDTO.setCreatedOn( entity.getCreatedOn() );
        jobDTO.setUpdatedOn( entity.getUpdatedOn() );
        jobDTO.setDomain( entity.getDomain() );
        jobDTO.setClient( entity.getClient() );
        jobDTO.setCountry( entity.getCountry() );

        return jobDTO;
    }

    @Override
    public List<Job> toEntity(List<JobDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Job> list = new ArrayList<Job>( dtoList.size() );
        for ( JobDTO jobDTO : dtoList ) {
            list.add( toEntity( jobDTO ) );
        }

        return list;
    }

    @Override
    public List<JobDTO> toDto(List<Job> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<JobDTO> list = new ArrayList<JobDTO>( entityList.size() );
        for ( Job job : entityList ) {
            list.add( toDto( job ) );
        }

        return list;
    }
}
