package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.Message;
import com.kizkom.upartna.service.dto.MessageDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-10-21T17:56:50+0200",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_201 (Oracle Corporation)"
)
@Component
public class MessageMapperImpl implements MessageMapper {

    @Override
    public Message toEntity(MessageDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Message message = new Message();

        message.setId( dto.getId() );
        message.setBody( dto.getBody() );
        message.setSendingDate( dto.getSendingDate() );
        message.setReceivingDate( dto.getReceivingDate() );
        message.setSender( dto.getSender() );
        message.setReceiver( dto.getReceiver() );

        return message;
    }

    @Override
    public MessageDTO toDto(Message entity) {
        if ( entity == null ) {
            return null;
        }

        MessageDTO messageDTO = new MessageDTO();

        messageDTO.setId( entity.getId() );
        messageDTO.setBody( entity.getBody() );
        messageDTO.setSendingDate( entity.getSendingDate() );
        messageDTO.setReceivingDate( entity.getReceivingDate() );
        messageDTO.setSender( entity.getSender() );
        messageDTO.setReceiver( entity.getReceiver() );

        return messageDTO;
    }

    @Override
    public List<Message> toEntity(List<MessageDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Message> list = new ArrayList<Message>( dtoList.size() );
        for ( MessageDTO messageDTO : dtoList ) {
            list.add( toEntity( messageDTO ) );
        }

        return list;
    }

    @Override
    public List<MessageDTO> toDto(List<Message> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<MessageDTO> list = new ArrayList<MessageDTO>( entityList.size() );
        for ( Message message : entityList ) {
            list.add( toDto( message ) );
        }

        return list;
    }
}
