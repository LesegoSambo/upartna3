package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.Notification;
import com.kizkom.upartna.service.dto.NotificationDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-10-21T17:56:50+0200",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_201 (Oracle Corporation)"
)
@Component
public class NotificationMapperImpl implements NotificationMapper {

    @Override
    public Notification toEntity(NotificationDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Notification notification = new Notification();

        notification.setId( dto.getId() );
        notification.setTitle( dto.getTitle() );
        notification.setMessage( dto.getMessage() );
        notification.setSendingDate( dto.getSendingDate() );
        notification.setReceivingDate( dto.getReceivingDate() );
        notification.setViewDate( dto.getViewDate() );
        notification.setIsDeleted( dto.isIsDeleted() );
        notification.setUser( dto.getUser() );

        return notification;
    }

    @Override
    public NotificationDTO toDto(Notification entity) {
        if ( entity == null ) {
            return null;
        }

        NotificationDTO notificationDTO = new NotificationDTO();

        notificationDTO.setId( entity.getId() );
        notificationDTO.setTitle( entity.getTitle() );
        notificationDTO.setMessage( entity.getMessage() );
        notificationDTO.setSendingDate( entity.getSendingDate() );
        notificationDTO.setReceivingDate( entity.getReceivingDate() );
        notificationDTO.setViewDate( entity.getViewDate() );
        notificationDTO.setIsDeleted( entity.isIsDeleted() );
        notificationDTO.setUser( entity.getUser() );

        return notificationDTO;
    }

    @Override
    public List<Notification> toEntity(List<NotificationDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Notification> list = new ArrayList<Notification>( dtoList.size() );
        for ( NotificationDTO notificationDTO : dtoList ) {
            list.add( toEntity( notificationDTO ) );
        }

        return list;
    }

    @Override
    public List<NotificationDTO> toDto(List<Notification> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<NotificationDTO> list = new ArrayList<NotificationDTO>( entityList.size() );
        for ( Notification notification : entityList ) {
            list.add( toDto( notification ) );
        }

        return list;
    }
}
