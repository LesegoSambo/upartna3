package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.ProofFile;
import com.kizkom.upartna.service.dto.ProofFileDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-10-21T17:56:50+0200",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_201 (Oracle Corporation)"
)
@Component
public class ProofFileMapperImpl implements ProofFileMapper {

    @Override
    public ProofFile toEntity(ProofFileDTO dto) {
        if ( dto == null ) {
            return null;
        }

        ProofFile proofFile = new ProofFile();

        proofFile.setId( dto.getId() );
        proofFile.setUrl( dto.getUrl() );
        proofFile.setFileType( dto.getFileType() );
        proofFile.setExtension( dto.getExtension() );
        proofFile.setProof( dto.getProof() );

        return proofFile;
    }

    @Override
    public ProofFileDTO toDto(ProofFile entity) {
        if ( entity == null ) {
            return null;
        }

        ProofFileDTO proofFileDTO = new ProofFileDTO();

        proofFileDTO.setId( entity.getId() );
        proofFileDTO.setUrl( entity.getUrl() );
        proofFileDTO.setFileType( entity.getFileType() );
        proofFileDTO.setExtension( entity.getExtension() );
        proofFileDTO.setProof( entity.getProof() );

        return proofFileDTO;
    }

    @Override
    public List<ProofFile> toEntity(List<ProofFileDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<ProofFile> list = new ArrayList<ProofFile>( dtoList.size() );
        for ( ProofFileDTO proofFileDTO : dtoList ) {
            list.add( toEntity( proofFileDTO ) );
        }

        return list;
    }

    @Override
    public List<ProofFileDTO> toDto(List<ProofFile> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<ProofFileDTO> list = new ArrayList<ProofFileDTO>( entityList.size() );
        for ( ProofFile proofFile : entityList ) {
            list.add( toDto( proofFile ) );
        }

        return list;
    }
}
