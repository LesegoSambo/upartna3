package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.Proof;
import com.kizkom.upartna.service.dto.ProofDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-10-21T17:56:50+0200",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_201 (Oracle Corporation)"
)
@Component
public class ProofMapperImpl implements ProofMapper {

    @Override
    public Proof toEntity(ProofDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Proof proof = new Proof();

        proof.setId( dto.getId() );
        proof.setTitle( dto.getTitle() );
        proof.setDescription( dto.getDescription() );
        proof.setCreatedOn( dto.getCreatedOn() );
        proof.setContract( dto.getContract() );

        return proof;
    }

    @Override
    public ProofDTO toDto(Proof entity) {
        if ( entity == null ) {
            return null;
        }

        ProofDTO proofDTO = new ProofDTO();

        proofDTO.setId( entity.getId() );
        proofDTO.setTitle( entity.getTitle() );
        proofDTO.setDescription( entity.getDescription() );
        proofDTO.setCreatedOn( entity.getCreatedOn() );
        proofDTO.setContract( entity.getContract() );

        return proofDTO;
    }

    @Override
    public List<Proof> toEntity(List<ProofDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Proof> list = new ArrayList<Proof>( dtoList.size() );
        for ( ProofDTO proofDTO : dtoList ) {
            list.add( toEntity( proofDTO ) );
        }

        return list;
    }

    @Override
    public List<ProofDTO> toDto(List<Proof> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<ProofDTO> list = new ArrayList<ProofDTO>( entityList.size() );
        for ( Proof proof : entityList ) {
            list.add( toDto( proof ) );
        }

        return list;
    }
}
