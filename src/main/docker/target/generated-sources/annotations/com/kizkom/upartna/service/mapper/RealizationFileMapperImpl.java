package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.RealizationFile;
import com.kizkom.upartna.service.dto.RealizationFileDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-10-21T17:56:50+0200",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_201 (Oracle Corporation)"
)
@Component
public class RealizationFileMapperImpl implements RealizationFileMapper {

    @Override
    public RealizationFile toEntity(RealizationFileDTO dto) {
        if ( dto == null ) {
            return null;
        }

        RealizationFile realizationFile = new RealizationFile();

        realizationFile.setId( dto.getId() );
        realizationFile.setRealization( dto.getRealization() );

        return realizationFile;
    }

    @Override
    public RealizationFileDTO toDto(RealizationFile entity) {
        if ( entity == null ) {
            return null;
        }

        RealizationFileDTO realizationFileDTO = new RealizationFileDTO();

        realizationFileDTO.setId( entity.getId() );
        realizationFileDTO.setRealization( entity.getRealization() );

        return realizationFileDTO;
    }

    @Override
    public List<RealizationFile> toEntity(List<RealizationFileDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<RealizationFile> list = new ArrayList<RealizationFile>( dtoList.size() );
        for ( RealizationFileDTO realizationFileDTO : dtoList ) {
            list.add( toEntity( realizationFileDTO ) );
        }

        return list;
    }

    @Override
    public List<RealizationFileDTO> toDto(List<RealizationFile> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<RealizationFileDTO> list = new ArrayList<RealizationFileDTO>( entityList.size() );
        for ( RealizationFile realizationFile : entityList ) {
            list.add( toDto( realizationFile ) );
        }

        return list;
    }
}
