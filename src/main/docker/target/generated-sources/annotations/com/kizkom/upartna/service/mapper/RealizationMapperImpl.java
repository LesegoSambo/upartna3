package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.Realization;
import com.kizkom.upartna.service.dto.RealizationDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-10-21T17:56:50+0200",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_201 (Oracle Corporation)"
)
@Component
public class RealizationMapperImpl implements RealizationMapper {

    @Override
    public Realization toEntity(RealizationDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Realization realization = new Realization();

        realization.setId( dto.getId() );
        realization.setTitle( dto.getTitle() );
        realization.setDescription( dto.getDescription() );
        realization.setRealizationDate( dto.getRealizationDate() );
        realization.setLocation( dto.getLocation() );
        realization.setExpert( dto.getExpert() );

        return realization;
    }

    @Override
    public RealizationDTO toDto(Realization entity) {
        if ( entity == null ) {
            return null;
        }

        RealizationDTO realizationDTO = new RealizationDTO();

        realizationDTO.setId( entity.getId() );
        realizationDTO.setTitle( entity.getTitle() );
        realizationDTO.setDescription( entity.getDescription() );
        realizationDTO.setRealizationDate( entity.getRealizationDate() );
        realizationDTO.setLocation( entity.getLocation() );
        realizationDTO.setExpert( entity.getExpert() );

        return realizationDTO;
    }

    @Override
    public List<Realization> toEntity(List<RealizationDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Realization> list = new ArrayList<Realization>( dtoList.size() );
        for ( RealizationDTO realizationDTO : dtoList ) {
            list.add( toEntity( realizationDTO ) );
        }

        return list;
    }

    @Override
    public List<RealizationDTO> toDto(List<Realization> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<RealizationDTO> list = new ArrayList<RealizationDTO>( entityList.size() );
        for ( Realization realization : entityList ) {
            list.add( toDto( realization ) );
        }

        return list;
    }
}
