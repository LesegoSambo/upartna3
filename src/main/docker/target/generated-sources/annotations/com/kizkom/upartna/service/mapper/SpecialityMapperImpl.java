package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.Speciality;
import com.kizkom.upartna.service.dto.SpecialityDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-10-21T17:56:50+0200",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_201 (Oracle Corporation)"
)
@Component
public class SpecialityMapperImpl implements SpecialityMapper {

    @Override
    public Speciality toEntity(SpecialityDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Speciality speciality = new Speciality();

        speciality.setId( dto.getId() );
        speciality.setTitle( dto.getTitle() );
        speciality.setDescription( dto.getDescription() );

        return speciality;
    }

    @Override
    public SpecialityDTO toDto(Speciality entity) {
        if ( entity == null ) {
            return null;
        }

        SpecialityDTO specialityDTO = new SpecialityDTO();

        specialityDTO.setId( entity.getId() );
        specialityDTO.setTitle( entity.getTitle() );
        specialityDTO.setDescription( entity.getDescription() );

        return specialityDTO;
    }

    @Override
    public List<Speciality> toEntity(List<SpecialityDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Speciality> list = new ArrayList<Speciality>( dtoList.size() );
        for ( SpecialityDTO specialityDTO : dtoList ) {
            list.add( toEntity( specialityDTO ) );
        }

        return list;
    }

    @Override
    public List<SpecialityDTO> toDto(List<Speciality> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<SpecialityDTO> list = new ArrayList<SpecialityDTO>( entityList.size() );
        for ( Speciality speciality : entityList ) {
            list.add( toDto( speciality ) );
        }

        return list;
    }
}
