package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.SubmissionFile;
import com.kizkom.upartna.service.dto.SubmissionFileDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-10-21T17:56:50+0200",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_201 (Oracle Corporation)"
)
@Component
public class SubmissionFileMapperImpl implements SubmissionFileMapper {

    @Override
    public SubmissionFile toEntity(SubmissionFileDTO dto) {
        if ( dto == null ) {
            return null;
        }

        SubmissionFile submissionFile = new SubmissionFile();

        submissionFile.setId( dto.getId() );
        submissionFile.setSubmission( dto.getSubmission() );

        return submissionFile;
    }

    @Override
    public SubmissionFileDTO toDto(SubmissionFile entity) {
        if ( entity == null ) {
            return null;
        }

        SubmissionFileDTO submissionFileDTO = new SubmissionFileDTO();

        submissionFileDTO.setId( entity.getId() );
        submissionFileDTO.setSubmission( entity.getSubmission() );

        return submissionFileDTO;
    }

    @Override
    public List<SubmissionFile> toEntity(List<SubmissionFileDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<SubmissionFile> list = new ArrayList<SubmissionFile>( dtoList.size() );
        for ( SubmissionFileDTO submissionFileDTO : dtoList ) {
            list.add( toEntity( submissionFileDTO ) );
        }

        return list;
    }

    @Override
    public List<SubmissionFileDTO> toDto(List<SubmissionFile> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<SubmissionFileDTO> list = new ArrayList<SubmissionFileDTO>( entityList.size() );
        for ( SubmissionFile submissionFile : entityList ) {
            list.add( toDto( submissionFile ) );
        }

        return list;
    }
}
