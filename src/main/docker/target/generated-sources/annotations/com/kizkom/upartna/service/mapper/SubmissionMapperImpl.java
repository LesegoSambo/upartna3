package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.Submission;
import com.kizkom.upartna.service.dto.SubmissionDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-10-21T17:56:50+0200",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_201 (Oracle Corporation)"
)
@Component
public class SubmissionMapperImpl implements SubmissionMapper {

    @Override
    public Submission toEntity(SubmissionDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Submission submission = new Submission();

        submission.setId( dto.getId() );
        submission.setSubmitedOn( dto.getSubmitedOn() );
        submission.setDescription( dto.getDescription() );
        submission.setAmount( dto.getAmount() );
        submission.setUser( dto.getUser() );
        submission.setJob( dto.getJob() );

        return submission;
    }

    @Override
    public SubmissionDTO toDto(Submission entity) {
        if ( entity == null ) {
            return null;
        }

        SubmissionDTO submissionDTO = new SubmissionDTO();

        submissionDTO.setId( entity.getId() );
        submissionDTO.setSubmitedOn( entity.getSubmitedOn() );
        submissionDTO.setDescription( entity.getDescription() );
        submissionDTO.setAmount( entity.getAmount() );
        submissionDTO.setUser( entity.getUser() );
        submissionDTO.setJob( entity.getJob() );

        return submissionDTO;
    }

    @Override
    public List<Submission> toEntity(List<SubmissionDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Submission> list = new ArrayList<Submission>( dtoList.size() );
        for ( SubmissionDTO submissionDTO : dtoList ) {
            list.add( toEntity( submissionDTO ) );
        }

        return list;
    }

    @Override
    public List<SubmissionDTO> toDto(List<Submission> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<SubmissionDTO> list = new ArrayList<SubmissionDTO>( entityList.size() );
        for ( Submission submission : entityList ) {
            list.add( toDto( submission ) );
        }

        return list;
    }
}
