(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .controller('ContractFileDetailController', ContractFileDetailController);

    ContractFileDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ContractFile'];

    function ContractFileDetailController($scope, $rootScope, $stateParams, previousState, entity, ContractFile) {
        var vm = this;

        vm.contractFile = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('upartnaApp:contractFileUpdate', function (event, result) {
            vm.contractFile = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
