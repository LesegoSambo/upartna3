(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .controller('ContractFileDialogController', ContractFileDialogController);

    ContractFileDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ContractFile'];

    function ContractFileDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, ContractFile) {
        var vm = this;

        vm.contractFile = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.contractFile.id !== null) {
                ContractFile.update(vm.contractFile, onSaveSuccess, onSaveError);
            } else {
                ContractFile.save(vm.contractFile, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $scope.$emit('upartnaApp:contractFileUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }


    }
})();
