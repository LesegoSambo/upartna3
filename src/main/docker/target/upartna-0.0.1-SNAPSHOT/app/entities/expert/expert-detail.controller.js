(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .controller('ExpertDetailController', ExpertDetailController);

    ExpertDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Expert'];

    function ExpertDetailController($scope, $rootScope, $stateParams, previousState, entity, Expert) {
        var vm = this;

        vm.expert = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('upartnaApp:expertUpdate', function (event, result) {
            vm.expert = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
