(function () {
    'use strict';
    angular
        .module('upartnaApp')
        .factory('Expert', Expert);

    Expert.$inject = ['$resource', 'DateUtils'];

    function Expert($resource, DateUtils) {
        var resourceUrl = 'api/experts/:id';

        return $resource(resourceUrl, {}, {
            'query': {method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.experienceStartDate = DateUtils.convertLocalDateFromServer(data.experienceStartDate);
                        data.activatedOn = DateUtils.convertLocalDateFromServer(data.activatedOn);
                        data.createdOn = DateUtils.convertLocalDateFromServer(data.createdOn);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.experienceStartDate = DateUtils.convertLocalDateToServer(copy.experienceStartDate);
                    copy.activatedOn = DateUtils.convertLocalDateToServer(copy.activatedOn);
                    copy.createdOn = DateUtils.convertLocalDateToServer(copy.createdOn);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.experienceStartDate = DateUtils.convertLocalDateToServer(copy.experienceStartDate);
                    copy.activatedOn = DateUtils.convertLocalDateToServer(copy.activatedOn);
                    copy.createdOn = DateUtils.convertLocalDateToServer(copy.createdOn);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
