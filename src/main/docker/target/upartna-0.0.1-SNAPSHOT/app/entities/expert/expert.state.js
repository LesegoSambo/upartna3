(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('expert', {
                parent: 'entity',
                url: '/expert?page&sort&search',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'upartnaApp.expert.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/expert/experts.html',
                        controller: 'ExpertController',
                        controllerAs: 'vm'
                    }
                },
                params: {
                    page: {
                        value: '1',
                        squash: true
                    },
                    sort: {
                        value: 'id,asc',
                        squash: true
                    },
                    search: null
                },
                resolve: {
                    pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                        return {
                            page: PaginationUtil.parsePage($stateParams.page),
                            sort: $stateParams.sort,
                            predicate: PaginationUtil.parsePredicate($stateParams.sort),
                            ascending: PaginationUtil.parseAscending($stateParams.sort),
                            search: $stateParams.search
                        };
                    }],
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('expert');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('expert-detail', {
                parent: 'expert',
                url: '/expert/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'upartnaApp.expert.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/expert/expert-detail.html',
                        controller: 'ExpertDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('expert');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Expert', function ($stateParams, Expert) {
                        return Expert.get({id: $stateParams.id}).$promise;
                    }],
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'expert',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('expert-detail.edit', {
                parent: 'expert-detail',
                url: '/detail/edit',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/expert/expert-dialog.html',
                        controller: 'ExpertDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Expert', function (Expert) {
                                return Expert.get({id: $stateParams.id}).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('^', {}, {reload: false});
                    }, function () {
                        $state.go('^');
                    });
                }]
            })
            .state('expert.new', {
                parent: 'expert',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/expert/expert-dialog.html',
                        controller: 'ExpertDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    title: null,
                                    about: null,
                                    website: null,
                                    isActivated: null,
                                    isCompany: null,
                                    taxPayerNumber: null,
                                    experienceStartDate: null,
                                    activatedOn: null,
                                    createdOn: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function () {
                        $state.go('expert', null, {reload: 'expert'});
                    }, function () {
                        $state.go('expert');
                    });
                }]
            })
            .state('expert.edit', {
                parent: 'expert',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/expert/expert-dialog.html',
                        controller: 'ExpertDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Expert', function (Expert) {
                                return Expert.get({id: $stateParams.id}).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('expert', null, {reload: 'expert'});
                    }, function () {
                        $state.go('^');
                    });
                }]
            })
            .state('expert.delete', {
                parent: 'expert',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/expert/expert-delete-dialog.html',
                        controller: 'ExpertDeleteController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            entity: ['Expert', function (Expert) {
                                return Expert.get({id: $stateParams.id}).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('expert', null, {reload: 'expert'});
                    }, function () {
                        $state.go('^');
                    });
                }]
            });
    }

})();
