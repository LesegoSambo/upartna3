(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .controller('JobFileDetailController', JobFileDetailController);

    JobFileDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'JobFile'];

    function JobFileDetailController($scope, $rootScope, $stateParams, previousState, entity, JobFile) {
        var vm = this;

        vm.jobFile = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('upartnaApp:jobFileUpdate', function (event, result) {
            vm.jobFile = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
