(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('proof-file', {
                parent: 'entity',
                url: '/proof-file?page&sort&search',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'upartnaApp.proofFile.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/proof-file/proof-files.html',
                        controller: 'ProofFileController',
                        controllerAs: 'vm'
                    }
                },
                params: {
                    page: {
                        value: '1',
                        squash: true
                    },
                    sort: {
                        value: 'id,asc',
                        squash: true
                    },
                    search: null
                },
                resolve: {
                    pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                        return {
                            page: PaginationUtil.parsePage($stateParams.page),
                            sort: $stateParams.sort,
                            predicate: PaginationUtil.parsePredicate($stateParams.sort),
                            ascending: PaginationUtil.parseAscending($stateParams.sort),
                            search: $stateParams.search
                        };
                    }],
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('proofFile');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('proof-file-detail', {
                parent: 'proof-file',
                url: '/proof-file/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'upartnaApp.proofFile.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/proof-file/proof-file-detail.html',
                        controller: 'ProofFileDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('proofFile');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'ProofFile', function ($stateParams, ProofFile) {
                        return ProofFile.get({id: $stateParams.id}).$promise;
                    }],
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'proof-file',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('proof-file-detail.edit', {
                parent: 'proof-file-detail',
                url: '/detail/edit',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/proof-file/proof-file-dialog.html',
                        controller: 'ProofFileDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['ProofFile', function (ProofFile) {
                                return ProofFile.get({id: $stateParams.id}).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('^', {}, {reload: false});
                    }, function () {
                        $state.go('^');
                    });
                }]
            })
            .state('proof-file.new', {
                parent: 'proof-file',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/proof-file/proof-file-dialog.html',
                        controller: 'ProofFileDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    url: null,
                                    fileType: null,
                                    extension: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function () {
                        $state.go('proof-file', null, {reload: 'proof-file'});
                    }, function () {
                        $state.go('proof-file');
                    });
                }]
            })
            .state('proof-file.edit', {
                parent: 'proof-file',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/proof-file/proof-file-dialog.html',
                        controller: 'ProofFileDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['ProofFile', function (ProofFile) {
                                return ProofFile.get({id: $stateParams.id}).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('proof-file', null, {reload: 'proof-file'});
                    }, function () {
                        $state.go('^');
                    });
                }]
            })
            .state('proof-file.delete', {
                parent: 'proof-file',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/proof-file/proof-file-delete-dialog.html',
                        controller: 'ProofFileDeleteController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            entity: ['ProofFile', function (ProofFile) {
                                return ProofFile.get({id: $stateParams.id}).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('proof-file', null, {reload: 'proof-file'});
                    }, function () {
                        $state.go('^');
                    });
                }]
            });
    }

})();
