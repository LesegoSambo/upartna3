(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .controller('RealizationFileDeleteController', RealizationFileDeleteController);

    RealizationFileDeleteController.$inject = ['$uibModalInstance', 'entity', 'RealizationFile'];

    function RealizationFileDeleteController($uibModalInstance, entity, RealizationFile) {
        var vm = this;

        vm.realizationFile = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete(id) {
            RealizationFile.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
