(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .controller('RealizationFileDetailController', RealizationFileDetailController);

    RealizationFileDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'RealizationFile'];

    function RealizationFileDetailController($scope, $rootScope, $stateParams, previousState, entity, RealizationFile) {
        var vm = this;

        vm.realizationFile = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('upartnaApp:realizationFileUpdate', function (event, result) {
            vm.realizationFile = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
