(function () {
    'use strict';
    angular
        .module('upartnaApp')
        .factory('RealizationFile', RealizationFile);

    RealizationFile.$inject = ['$resource'];

    function RealizationFile($resource) {
        var resourceUrl = 'api/realization-files/:id';

        return $resource(resourceUrl, {}, {
            'query': {method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': {method: 'PUT'}
        });
    }
})();
