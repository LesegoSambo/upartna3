(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .controller('RealizationDeleteController', RealizationDeleteController);

    RealizationDeleteController.$inject = ['$uibModalInstance', 'entity', 'Realization'];

    function RealizationDeleteController($uibModalInstance, entity, Realization) {
        var vm = this;

        vm.realization = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete(id) {
            Realization.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
