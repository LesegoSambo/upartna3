(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .controller('RealizationDetailController', RealizationDetailController);

    RealizationDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Realization'];

    function RealizationDetailController($scope, $rootScope, $stateParams, previousState, entity, Realization) {
        var vm = this;

        vm.realization = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('upartnaApp:realizationUpdate', function (event, result) {
            vm.realization = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
