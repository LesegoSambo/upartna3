(function () {
    'use strict';
    angular
        .module('upartnaApp')
        .factory('Realization', Realization);

    Realization.$inject = ['$resource', 'DateUtils'];

    function Realization($resource, DateUtils) {
        var resourceUrl = 'api/realizations/:id';

        return $resource(resourceUrl, {}, {
            'query': {method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.realizationDate = DateUtils.convertLocalDateFromServer(data.realizationDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.realizationDate = DateUtils.convertLocalDateToServer(copy.realizationDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.realizationDate = DateUtils.convertLocalDateToServer(copy.realizationDate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
