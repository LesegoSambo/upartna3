(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .controller('SpecialityDetailController', SpecialityDetailController);

    SpecialityDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Speciality'];

    function SpecialityDetailController($scope, $rootScope, $stateParams, previousState, entity, Speciality) {
        var vm = this;

        vm.speciality = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('upartnaApp:specialityUpdate', function (event, result) {
            vm.speciality = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
