(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .controller('SubmissionFileDeleteController', SubmissionFileDeleteController);

    SubmissionFileDeleteController.$inject = ['$uibModalInstance', 'entity', 'SubmissionFile'];

    function SubmissionFileDeleteController($uibModalInstance, entity, SubmissionFile) {
        var vm = this;

        vm.submissionFile = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete(id) {
            SubmissionFile.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
