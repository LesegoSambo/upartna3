(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('submission-file', {
                parent: 'entity',
                url: '/submission-file?page&sort&search',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'upartnaApp.submissionFile.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/submission-file/submission-files.html',
                        controller: 'SubmissionFileController',
                        controllerAs: 'vm'
                    }
                },
                params: {
                    page: {
                        value: '1',
                        squash: true
                    },
                    sort: {
                        value: 'id,asc',
                        squash: true
                    },
                    search: null
                },
                resolve: {
                    pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                        return {
                            page: PaginationUtil.parsePage($stateParams.page),
                            sort: $stateParams.sort,
                            predicate: PaginationUtil.parsePredicate($stateParams.sort),
                            ascending: PaginationUtil.parseAscending($stateParams.sort),
                            search: $stateParams.search
                        };
                    }],
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('submissionFile');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('submission-file-detail', {
                parent: 'submission-file',
                url: '/submission-file/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'upartnaApp.submissionFile.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/submission-file/submission-file-detail.html',
                        controller: 'SubmissionFileDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('submissionFile');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'SubmissionFile', function ($stateParams, SubmissionFile) {
                        return SubmissionFile.get({id: $stateParams.id}).$promise;
                    }],
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'submission-file',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('submission-file-detail.edit', {
                parent: 'submission-file-detail',
                url: '/detail/edit',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/submission-file/submission-file-dialog.html',
                        controller: 'SubmissionFileDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['SubmissionFile', function (SubmissionFile) {
                                return SubmissionFile.get({id: $stateParams.id}).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('^', {}, {reload: false});
                    }, function () {
                        $state.go('^');
                    });
                }]
            })
            .state('submission-file.new', {
                parent: 'submission-file',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/submission-file/submission-file-dialog.html',
                        controller: 'SubmissionFileDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    id: null
                                };
                            }
                        }
                    }).result.then(function () {
                        $state.go('submission-file', null, {reload: 'submission-file'});
                    }, function () {
                        $state.go('submission-file');
                    });
                }]
            })
            .state('submission-file.edit', {
                parent: 'submission-file',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/submission-file/submission-file-dialog.html',
                        controller: 'SubmissionFileDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['SubmissionFile', function (SubmissionFile) {
                                return SubmissionFile.get({id: $stateParams.id}).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('submission-file', null, {reload: 'submission-file'});
                    }, function () {
                        $state.go('^');
                    });
                }]
            })
            .state('submission-file.delete', {
                parent: 'submission-file',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/submission-file/submission-file-delete-dialog.html',
                        controller: 'SubmissionFileDeleteController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            entity: ['SubmissionFile', function (SubmissionFile) {
                                return SubmissionFile.get({id: $stateParams.id}).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('submission-file', null, {reload: 'submission-file'});
                    }, function () {
                        $state.go('^');
                    });
                }]
            });
    }

})();
