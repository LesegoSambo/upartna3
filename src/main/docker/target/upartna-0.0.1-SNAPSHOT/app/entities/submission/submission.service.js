(function () {
    'use strict';
    angular
        .module('upartnaApp')
        .factory('Submission', Submission);

    Submission.$inject = ['$resource', 'DateUtils'];

    function Submission($resource, DateUtils) {
        var resourceUrl = 'api/submissions/:id';

        return $resource(resourceUrl, {}, {
            'query': {method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.submitedOn = DateUtils.convertLocalDateFromServer(data.submitedOn);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.submitedOn = DateUtils.convertLocalDateToServer(copy.submitedOn);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.submitedOn = DateUtils.convertLocalDateToServer(copy.submitedOn);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
