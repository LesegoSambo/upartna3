package com.kizkom.upartna.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A RealizationFile.
 */
@Document(collection = "realization_file")
public class RealizationFile implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @DBRef
    @NotNull
    private Realization realization;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    public Realization getRealization() {
        return realization;
    }

    public void setRealization(Realization realization) {
        this.realization = realization;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RealizationFile realizationFile = (RealizationFile) o;
        if (realizationFile.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), realizationFile.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RealizationFile{" +
            "id=" + getId() +
            "}";
    }
}
