package com.kizkom.upartna.repository;

import com.kizkom.upartna.domain.ContractFile;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the ContractFile entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ContractFileRepository extends MongoRepository<ContractFile, String> {

}
