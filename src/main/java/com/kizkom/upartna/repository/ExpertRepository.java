package com.kizkom.upartna.repository;

import com.kizkom.upartna.domain.Expert;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Expert entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ExpertRepository extends MongoRepository<Expert, String> {

}
