package com.kizkom.upartna.repository;

import com.kizkom.upartna.domain.JobFile;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the JobFile entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JobFileRepository extends MongoRepository<JobFile, String> {

}
