package com.kizkom.upartna.repository;

import com.kizkom.upartna.domain.ProofFile;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the ProofFile entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProofFileRepository extends MongoRepository<ProofFile, String> {

}
