package com.kizkom.upartna.repository;

import com.kizkom.upartna.domain.Proof;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Proof entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProofRepository extends MongoRepository<Proof, String> {

}
