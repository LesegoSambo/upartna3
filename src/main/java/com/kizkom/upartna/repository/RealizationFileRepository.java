package com.kizkom.upartna.repository;

import com.kizkom.upartna.domain.RealizationFile;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the RealizationFile entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RealizationFileRepository extends MongoRepository<RealizationFile, String> {

}
