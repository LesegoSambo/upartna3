package com.kizkom.upartna.repository;

import com.kizkom.upartna.domain.Speciality;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Speciality entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SpecialityRepository extends MongoRepository<Speciality, String> {

}
