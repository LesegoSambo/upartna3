package com.kizkom.upartna.repository;

import com.kizkom.upartna.domain.SubmissionFile;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the SubmissionFile entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SubmissionFileRepository extends MongoRepository<SubmissionFile, String> {

}
