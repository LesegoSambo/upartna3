package com.kizkom.upartna.service;

import com.kizkom.upartna.service.dto.ContractFileDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing ContractFile.
 */
public interface ContractFileService {

    /**
     * Save a contractFile.
     *
     * @param contractFileDTO the entity to save
     * @return the persisted entity
     */
    ContractFileDTO save(ContractFileDTO contractFileDTO);

    /**
     * Get all the contractFiles.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<ContractFileDTO> findAll(Pageable pageable);

    /**
     * Get the "id" contractFile.
     *
     * @param id the id of the entity
     * @return the entity
     */
    ContractFileDTO findOne(String id);

    /**
     * Delete the "id" contractFile.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
