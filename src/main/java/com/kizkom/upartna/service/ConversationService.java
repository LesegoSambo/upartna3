package com.kizkom.upartna.service;

import com.kizkom.upartna.service.dto.ConversationDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Conversation.
 */
public interface ConversationService {

    /**
     * Save a conversation.
     *
     * @param conversationDTO the entity to save
     * @return the persisted entity
     */
    ConversationDTO save(ConversationDTO conversationDTO);

    /**
     * Get all the conversations.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<ConversationDTO> findAll(Pageable pageable);

    /**
     * Get the "id" conversation.
     *
     * @param id the id of the entity
     * @return the entity
     */
    ConversationDTO findOne(String id);

    /**
     * Delete the "id" conversation.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
