package com.kizkom.upartna.service;

import com.kizkom.upartna.service.dto.ExpertDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Expert.
 */
public interface ExpertService {

    /**
     * Save a expert.
     *
     * @param expertDTO the entity to save
     * @return the persisted entity
     */
    ExpertDTO save(ExpertDTO expertDTO);

    /**
     * Get all the experts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<ExpertDTO> findAll(Pageable pageable);

    /**
     * Get the "id" expert.
     *
     * @param id the id of the entity
     * @return the entity
     */
    ExpertDTO findOne(String id);

    /**
     * Delete the "id" expert.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
