package com.kizkom.upartna.service;

import com.kizkom.upartna.service.dto.ProofFileDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing ProofFile.
 */
public interface ProofFileService {

    /**
     * Save a proofFile.
     *
     * @param proofFileDTO the entity to save
     * @return the persisted entity
     */
    ProofFileDTO save(ProofFileDTO proofFileDTO);

    /**
     * Get all the proofFiles.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<ProofFileDTO> findAll(Pageable pageable);

    /**
     * Get the "id" proofFile.
     *
     * @param id the id of the entity
     * @return the entity
     */
    ProofFileDTO findOne(String id);

    /**
     * Delete the "id" proofFile.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
