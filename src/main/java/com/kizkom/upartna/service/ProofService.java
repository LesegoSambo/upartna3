package com.kizkom.upartna.service;

import com.kizkom.upartna.service.dto.ProofDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Proof.
 */
public interface ProofService {

    /**
     * Save a proof.
     *
     * @param proofDTO the entity to save
     * @return the persisted entity
     */
    ProofDTO save(ProofDTO proofDTO);

    /**
     * Get all the proofs.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<ProofDTO> findAll(Pageable pageable);

    /**
     * Get the "id" proof.
     *
     * @param id the id of the entity
     * @return the entity
     */
    ProofDTO findOne(String id);

    /**
     * Delete the "id" proof.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
