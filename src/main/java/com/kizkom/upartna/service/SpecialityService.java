package com.kizkom.upartna.service;

import com.kizkom.upartna.service.dto.SpecialityDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Speciality.
 */
public interface SpecialityService {

    /**
     * Save a speciality.
     *
     * @param specialityDTO the entity to save
     * @return the persisted entity
     */
    SpecialityDTO save(SpecialityDTO specialityDTO);

    /**
     * Get all the specialities.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<SpecialityDTO> findAll(Pageable pageable);

    /**
     * Get the "id" speciality.
     *
     * @param id the id of the entity
     * @return the entity
     */
    SpecialityDTO findOne(String id);

    /**
     * Delete the "id" speciality.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
