package com.kizkom.upartna.service;

import com.kizkom.upartna.service.dto.SubmissionDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Submission.
 */
public interface SubmissionService {

    /**
     * Save a submission.
     *
     * @param submissionDTO the entity to save
     * @return the persisted entity
     */
    SubmissionDTO save(SubmissionDTO submissionDTO);

    /**
     * Get all the submissions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<SubmissionDTO> findAll(Pageable pageable);

    /**
     * Get the "id" submission.
     *
     * @param id the id of the entity
     * @return the entity
     */
    SubmissionDTO findOne(String id);

    /**
     * Delete the "id" submission.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
