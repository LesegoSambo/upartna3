package com.kizkom.upartna.service.dto;


import com.kizkom.upartna.domain.Contract;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the ContractFile entity.
 */
public class ContractFileDTO implements Serializable {

    private String id;

    @NotNull
    private String url;

    private String fileType;

    private String extension;

    @NotNull
    private Contract contract;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ContractFileDTO contractFileDTO = (ContractFileDTO) o;
        if (contractFileDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), contractFileDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ContractFileDTO{" +
            "id=" + getId() +
            ", url='" + getUrl() + "'" +
            ", fileType='" + getFileType() + "'" +
            ", extension='" + getExtension() + "'" +
            "}";
    }
}
