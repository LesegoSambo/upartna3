package com.kizkom.upartna.service.dto;


import com.kizkom.upartna.domain.User;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Expert entity.
 */
public class ExpertDTO implements Serializable {

    private String id;

    private String title;

    private String about;

    private String website;

    private Boolean isActivated;

    private Boolean isCompany;

    private String taxPayerNumber;

    private LocalDate experienceStartDate;

    private LocalDate activatedOn;

    private LocalDate createdOn;

    @NotNull
    private User user;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Boolean isIsActivated() {
        return isActivated;
    }

    public void setIsActivated(Boolean isActivated) {
        this.isActivated = isActivated;
    }

    public Boolean isIsCompany() {
        return isCompany;
    }

    public void setIsCompany(Boolean isCompany) {
        this.isCompany = isCompany;
    }

    public String getTaxPayerNumber() {
        return taxPayerNumber;
    }

    public void setTaxPayerNumber(String taxPayerNumber) {
        this.taxPayerNumber = taxPayerNumber;
    }

    public LocalDate getExperienceStartDate() {
        return experienceStartDate;
    }

    public void setExperienceStartDate(LocalDate experienceStartDate) {
        this.experienceStartDate = experienceStartDate;
    }

    public LocalDate getActivatedOn() {
        return activatedOn;
    }

    public void setActivatedOn(LocalDate activatedOn) {
        this.activatedOn = activatedOn;
    }

    public LocalDate getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDate createdOn) {
        this.createdOn = createdOn;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ExpertDTO expertDTO = (ExpertDTO) o;
        if (expertDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), expertDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ExpertDTO{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", about='" + getAbout() + "'" +
            ", website='" + getWebsite() + "'" +
            ", isActivated='" + isIsActivated() + "'" +
            ", isCompany='" + isIsCompany() + "'" +
            ", taxPayerNumber='" + getTaxPayerNumber() + "'" +
            ", experienceStartDate='" + getExperienceStartDate() + "'" +
            ", activatedOn='" + getActivatedOn() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            "}";
    }
}
