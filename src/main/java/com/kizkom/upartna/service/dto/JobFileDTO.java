package com.kizkom.upartna.service.dto;


import com.kizkom.upartna.domain.Job;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the JobFile entity.
 */
public class JobFileDTO implements Serializable {

    private String id;

    private String url;

    private String fileType;

    private String extension;

    @NotNull
    private Job job;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        JobFileDTO jobFileDTO = (JobFileDTO) o;
        if (jobFileDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), jobFileDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "JobFileDTO{" +
            "id=" + getId() +
            ", url='" + getUrl() + "'" +
            ", fileType='" + getFileType() + "'" +
            ", extension='" + getExtension() + "'" +
            "}";
    }
}
