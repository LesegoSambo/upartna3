package com.kizkom.upartna.service.dto;


import com.kizkom.upartna.domain.Expert;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Realization entity.
 */
public class RealizationDTO implements Serializable {

    private String id;

    @NotNull
    private String title;

    private String description;

    private LocalDate realizationDate;

    private String location;

    @NotNull
    private Expert expert;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getRealizationDate() {
        return realizationDate;
    }

    public void setRealizationDate(LocalDate realizationDate) {
        this.realizationDate = realizationDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Expert getExpert() {
        return expert;
    }

    public void setExpert(Expert expert) {
        this.expert = expert;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RealizationDTO realizationDTO = (RealizationDTO) o;
        if (realizationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), realizationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RealizationDTO{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            ", realizationDate='" + getRealizationDate() + "'" +
            ", location='" + getLocation() + "'" +
            "}";
    }
}
