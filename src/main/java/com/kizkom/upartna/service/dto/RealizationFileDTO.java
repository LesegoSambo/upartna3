package com.kizkom.upartna.service.dto;


import com.kizkom.upartna.domain.Realization;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the RealizationFile entity.
 */
public class RealizationFileDTO implements Serializable {

    private String id;

    @NotNull
    private Realization realization;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Realization getRealization() {
        return realization;
    }

    public void setRealization(Realization realization) {
        this.realization = realization;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RealizationFileDTO realizationFileDTO = (RealizationFileDTO) o;
        if (realizationFileDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), realizationFileDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RealizationFileDTO{" +
            "id=" + getId() +
            "}";
    }
}
