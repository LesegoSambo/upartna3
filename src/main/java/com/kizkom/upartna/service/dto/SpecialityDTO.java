package com.kizkom.upartna.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Speciality entity.
 */
public class SpecialityDTO implements Serializable {

    private String id;

    private String title;

    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SpecialityDTO specialityDTO = (SpecialityDTO) o;
        if (specialityDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), specialityDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SpecialityDTO{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
