package com.kizkom.upartna.service.dto;


import com.kizkom.upartna.domain.Job;
import com.kizkom.upartna.domain.User;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Submission entity.
 */
public class SubmissionDTO implements Serializable {

    private String id;

    private LocalDate submitedOn;

    private String description;

    private Double amount;

    @NotNull
    private User user;

    @NotNull
    private Job job;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDate getSubmitedOn() {
        return submitedOn;
    }

    public void setSubmitedOn(LocalDate submitedOn) {
        this.submitedOn = submitedOn;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SubmissionDTO submissionDTO = (SubmissionDTO) o;
        if (submissionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), submissionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SubmissionDTO{" +
            "id=" + getId() +
            ", submitedOn='" + getSubmitedOn() + "'" +
            ", description='" + getDescription() + "'" +
            ", amount=" + getAmount() +
            "}";
    }
}
