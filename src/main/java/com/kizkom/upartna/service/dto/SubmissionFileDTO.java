package com.kizkom.upartna.service.dto;


import com.kizkom.upartna.domain.Submission;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the SubmissionFile entity.
 */
public class SubmissionFileDTO implements Serializable {

    private String id;

    @NotNull
    private Submission submission;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Submission getSubmission() {
        return submission;
    }

    public void setSubmission(Submission submission) {
        this.submission = submission;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SubmissionFileDTO submissionFileDTO = (SubmissionFileDTO) o;
        if (submissionFileDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), submissionFileDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SubmissionFileDTO{" +
            "id=" + getId() +
            "}";
    }
}
