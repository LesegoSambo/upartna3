package com.kizkom.upartna.service.impl;

import com.kizkom.upartna.service.ContractFileService;
import com.kizkom.upartna.domain.ContractFile;
import com.kizkom.upartna.repository.ContractFileRepository;
import com.kizkom.upartna.service.dto.ContractFileDTO;
import com.kizkom.upartna.service.mapper.ContractFileMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


/**
 * Service Implementation for managing ContractFile.
 */
@Service
public class ContractFileServiceImpl implements ContractFileService {

    private final Logger log = LoggerFactory.getLogger(ContractFileServiceImpl.class);

    private final ContractFileRepository contractFileRepository;

    private final ContractFileMapper contractFileMapper;

    public ContractFileServiceImpl(ContractFileRepository contractFileRepository, ContractFileMapper contractFileMapper) {
        this.contractFileRepository = contractFileRepository;
        this.contractFileMapper = contractFileMapper;
    }

    /**
     * Save a contractFile.
     *
     * @param contractFileDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ContractFileDTO save(ContractFileDTO contractFileDTO) {
        log.debug("Request to save ContractFile : {}", contractFileDTO);
        ContractFile contractFile = contractFileMapper.toEntity(contractFileDTO);
        contractFile = contractFileRepository.save(contractFile);
        return contractFileMapper.toDto(contractFile);
    }

    /**
     * Get all the contractFiles.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<ContractFileDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ContractFiles");
        return contractFileRepository.findAll(pageable)
            .map(contractFileMapper::toDto);
    }

    /**
     * Get one contractFile by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public ContractFileDTO findOne(String id) {
        log.debug("Request to get ContractFile : {}", id);
        ContractFile contractFile = contractFileRepository.findOne(id);
        return contractFileMapper.toDto(contractFile);
    }

    /**
     * Delete the contractFile by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete ContractFile : {}", id);
        contractFileRepository.delete(id);
    }
}
