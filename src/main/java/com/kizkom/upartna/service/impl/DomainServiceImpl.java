package com.kizkom.upartna.service.impl;

import com.kizkom.upartna.service.DomainService;
import com.kizkom.upartna.domain.Domain;
import com.kizkom.upartna.repository.DomainRepository;
import com.kizkom.upartna.service.dto.DomainDTO;
import com.kizkom.upartna.service.mapper.DomainMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


/**
 * Service Implementation for managing Domain.
 */
@Service
public class DomainServiceImpl implements DomainService {

    private final Logger log = LoggerFactory.getLogger(DomainServiceImpl.class);

    private final DomainRepository domainRepository;

    private final DomainMapper domainMapper;

    public DomainServiceImpl(DomainRepository domainRepository, DomainMapper domainMapper) {
        this.domainRepository = domainRepository;
        this.domainMapper = domainMapper;
    }

    /**
     * Save a domain.
     *
     * @param domainDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public DomainDTO save(DomainDTO domainDTO) {
        log.debug("Request to save Domain : {}", domainDTO);
        Domain domain = domainMapper.toEntity(domainDTO);
        domain = domainRepository.save(domain);
        return domainMapper.toDto(domain);
    }

    /**
     * Get all the domains.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<DomainDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Domains");
        return domainRepository.findAll(pageable)
            .map(domainMapper::toDto);
    }

    /**
     * Get one domain by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public DomainDTO findOne(String id) {
        log.debug("Request to get Domain : {}", id);
        Domain domain = domainRepository.findOne(id);
        return domainMapper.toDto(domain);
    }

    /**
     * Delete the domain by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Domain : {}", id);
        domainRepository.delete(id);
    }
}
