package com.kizkom.upartna.service.impl;

import com.kizkom.upartna.service.ExpertService;
import com.kizkom.upartna.domain.Expert;
import com.kizkom.upartna.repository.ExpertRepository;
import com.kizkom.upartna.service.dto.ExpertDTO;
import com.kizkom.upartna.service.mapper.ExpertMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


/**
 * Service Implementation for managing Expert.
 */
@Service
public class ExpertServiceImpl implements ExpertService {

    private final Logger log = LoggerFactory.getLogger(ExpertServiceImpl.class);

    private final ExpertRepository expertRepository;

    private final ExpertMapper expertMapper;

    public ExpertServiceImpl(ExpertRepository expertRepository, ExpertMapper expertMapper) {
        this.expertRepository = expertRepository;
        this.expertMapper = expertMapper;
    }

    /**
     * Save a expert.
     *
     * @param expertDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ExpertDTO save(ExpertDTO expertDTO) {
        log.debug("Request to save Expert : {}", expertDTO);
        Expert expert = expertMapper.toEntity(expertDTO);
        expert = expertRepository.save(expert);
        return expertMapper.toDto(expert);
    }

    /**
     * Get all the experts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<ExpertDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Experts");
        return expertRepository.findAll(pageable)
            .map(expertMapper::toDto);
    }

    /**
     * Get one expert by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public ExpertDTO findOne(String id) {
        log.debug("Request to get Expert : {}", id);
        Expert expert = expertRepository.findOne(id);
        return expertMapper.toDto(expert);
    }

    /**
     * Delete the expert by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Expert : {}", id);
        expertRepository.delete(id);
    }
}
