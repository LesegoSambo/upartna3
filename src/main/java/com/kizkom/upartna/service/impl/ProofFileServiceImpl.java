package com.kizkom.upartna.service.impl;

import com.kizkom.upartna.service.ProofFileService;
import com.kizkom.upartna.domain.ProofFile;
import com.kizkom.upartna.repository.ProofFileRepository;
import com.kizkom.upartna.service.dto.ProofFileDTO;
import com.kizkom.upartna.service.mapper.ProofFileMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


/**
 * Service Implementation for managing ProofFile.
 */
@Service
public class ProofFileServiceImpl implements ProofFileService {

    private final Logger log = LoggerFactory.getLogger(ProofFileServiceImpl.class);

    private final ProofFileRepository proofFileRepository;

    private final ProofFileMapper proofFileMapper;

    public ProofFileServiceImpl(ProofFileRepository proofFileRepository, ProofFileMapper proofFileMapper) {
        this.proofFileRepository = proofFileRepository;
        this.proofFileMapper = proofFileMapper;
    }

    /**
     * Save a proofFile.
     *
     * @param proofFileDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ProofFileDTO save(ProofFileDTO proofFileDTO) {
        log.debug("Request to save ProofFile : {}", proofFileDTO);
        ProofFile proofFile = proofFileMapper.toEntity(proofFileDTO);
        proofFile = proofFileRepository.save(proofFile);
        return proofFileMapper.toDto(proofFile);
    }

    /**
     * Get all the proofFiles.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<ProofFileDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProofFiles");
        return proofFileRepository.findAll(pageable)
            .map(proofFileMapper::toDto);
    }

    /**
     * Get one proofFile by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public ProofFileDTO findOne(String id) {
        log.debug("Request to get ProofFile : {}", id);
        ProofFile proofFile = proofFileRepository.findOne(id);
        return proofFileMapper.toDto(proofFile);
    }

    /**
     * Delete the proofFile by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete ProofFile : {}", id);
        proofFileRepository.delete(id);
    }
}
