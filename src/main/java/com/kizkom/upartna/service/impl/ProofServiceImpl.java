package com.kizkom.upartna.service.impl;

import com.kizkom.upartna.service.ProofService;
import com.kizkom.upartna.domain.Proof;
import com.kizkom.upartna.repository.ProofRepository;
import com.kizkom.upartna.service.dto.ProofDTO;
import com.kizkom.upartna.service.mapper.ProofMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


/**
 * Service Implementation for managing Proof.
 */
@Service
public class ProofServiceImpl implements ProofService {

    private final Logger log = LoggerFactory.getLogger(ProofServiceImpl.class);

    private final ProofRepository proofRepository;

    private final ProofMapper proofMapper;

    public ProofServiceImpl(ProofRepository proofRepository, ProofMapper proofMapper) {
        this.proofRepository = proofRepository;
        this.proofMapper = proofMapper;
    }

    /**
     * Save a proof.
     *
     * @param proofDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ProofDTO save(ProofDTO proofDTO) {
        log.debug("Request to save Proof : {}", proofDTO);
        Proof proof = proofMapper.toEntity(proofDTO);
        proof = proofRepository.save(proof);
        return proofMapper.toDto(proof);
    }

    /**
     * Get all the proofs.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<ProofDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Proofs");
        return proofRepository.findAll(pageable)
            .map(proofMapper::toDto);
    }

    /**
     * Get one proof by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public ProofDTO findOne(String id) {
        log.debug("Request to get Proof : {}", id);
        Proof proof = proofRepository.findOne(id);
        return proofMapper.toDto(proof);
    }

    /**
     * Delete the proof by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Proof : {}", id);
        proofRepository.delete(id);
    }
}
