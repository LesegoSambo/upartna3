package com.kizkom.upartna.service.impl;

import com.kizkom.upartna.service.RealizationFileService;
import com.kizkom.upartna.domain.RealizationFile;
import com.kizkom.upartna.repository.RealizationFileRepository;
import com.kizkom.upartna.service.dto.RealizationFileDTO;
import com.kizkom.upartna.service.mapper.RealizationFileMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


/**
 * Service Implementation for managing RealizationFile.
 */
@Service
public class RealizationFileServiceImpl implements RealizationFileService {

    private final Logger log = LoggerFactory.getLogger(RealizationFileServiceImpl.class);

    private final RealizationFileRepository realizationFileRepository;

    private final RealizationFileMapper realizationFileMapper;

    public RealizationFileServiceImpl(RealizationFileRepository realizationFileRepository, RealizationFileMapper realizationFileMapper) {
        this.realizationFileRepository = realizationFileRepository;
        this.realizationFileMapper = realizationFileMapper;
    }

    /**
     * Save a realizationFile.
     *
     * @param realizationFileDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public RealizationFileDTO save(RealizationFileDTO realizationFileDTO) {
        log.debug("Request to save RealizationFile : {}", realizationFileDTO);
        RealizationFile realizationFile = realizationFileMapper.toEntity(realizationFileDTO);
        realizationFile = realizationFileRepository.save(realizationFile);
        return realizationFileMapper.toDto(realizationFile);
    }

    /**
     * Get all the realizationFiles.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<RealizationFileDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RealizationFiles");
        return realizationFileRepository.findAll(pageable)
            .map(realizationFileMapper::toDto);
    }

    /**
     * Get one realizationFile by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public RealizationFileDTO findOne(String id) {
        log.debug("Request to get RealizationFile : {}", id);
        RealizationFile realizationFile = realizationFileRepository.findOne(id);
        return realizationFileMapper.toDto(realizationFile);
    }

    /**
     * Delete the realizationFile by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete RealizationFile : {}", id);
        realizationFileRepository.delete(id);
    }
}
