package com.kizkom.upartna.service.impl;

import com.kizkom.upartna.service.RealizationService;
import com.kizkom.upartna.domain.Realization;
import com.kizkom.upartna.repository.RealizationRepository;
import com.kizkom.upartna.service.dto.RealizationDTO;
import com.kizkom.upartna.service.mapper.RealizationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


/**
 * Service Implementation for managing Realization.
 */
@Service
public class RealizationServiceImpl implements RealizationService {

    private final Logger log = LoggerFactory.getLogger(RealizationServiceImpl.class);

    private final RealizationRepository realizationRepository;

    private final RealizationMapper realizationMapper;

    public RealizationServiceImpl(RealizationRepository realizationRepository, RealizationMapper realizationMapper) {
        this.realizationRepository = realizationRepository;
        this.realizationMapper = realizationMapper;
    }

    /**
     * Save a realization.
     *
     * @param realizationDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public RealizationDTO save(RealizationDTO realizationDTO) {
        log.debug("Request to save Realization : {}", realizationDTO);
        Realization realization = realizationMapper.toEntity(realizationDTO);
        realization = realizationRepository.save(realization);
        return realizationMapper.toDto(realization);
    }

    /**
     * Get all the realizations.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<RealizationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Realizations");
        return realizationRepository.findAll(pageable)
            .map(realizationMapper::toDto);
    }

    /**
     * Get one realization by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public RealizationDTO findOne(String id) {
        log.debug("Request to get Realization : {}", id);
        Realization realization = realizationRepository.findOne(id);
        return realizationMapper.toDto(realization);
    }

    /**
     * Delete the realization by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Realization : {}", id);
        realizationRepository.delete(id);
    }
}
