package com.kizkom.upartna.service.impl;

import com.kizkom.upartna.service.SubmissionFileService;
import com.kizkom.upartna.domain.SubmissionFile;
import com.kizkom.upartna.repository.SubmissionFileRepository;
import com.kizkom.upartna.service.dto.SubmissionFileDTO;
import com.kizkom.upartna.service.mapper.SubmissionFileMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


/**
 * Service Implementation for managing SubmissionFile.
 */
@Service
public class SubmissionFileServiceImpl implements SubmissionFileService {

    private final Logger log = LoggerFactory.getLogger(SubmissionFileServiceImpl.class);

    private final SubmissionFileRepository submissionFileRepository;

    private final SubmissionFileMapper submissionFileMapper;

    public SubmissionFileServiceImpl(SubmissionFileRepository submissionFileRepository, SubmissionFileMapper submissionFileMapper) {
        this.submissionFileRepository = submissionFileRepository;
        this.submissionFileMapper = submissionFileMapper;
    }

    /**
     * Save a submissionFile.
     *
     * @param submissionFileDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public SubmissionFileDTO save(SubmissionFileDTO submissionFileDTO) {
        log.debug("Request to save SubmissionFile : {}", submissionFileDTO);
        SubmissionFile submissionFile = submissionFileMapper.toEntity(submissionFileDTO);
        submissionFile = submissionFileRepository.save(submissionFile);
        return submissionFileMapper.toDto(submissionFile);
    }

    /**
     * Get all the submissionFiles.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<SubmissionFileDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SubmissionFiles");
        return submissionFileRepository.findAll(pageable)
            .map(submissionFileMapper::toDto);
    }

    /**
     * Get one submissionFile by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public SubmissionFileDTO findOne(String id) {
        log.debug("Request to get SubmissionFile : {}", id);
        SubmissionFile submissionFile = submissionFileRepository.findOne(id);
        return submissionFileMapper.toDto(submissionFile);
    }

    /**
     * Delete the submissionFile by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete SubmissionFile : {}", id);
        submissionFileRepository.delete(id);
    }
}
