package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.*;
import com.kizkom.upartna.service.dto.CommentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Comment and its DTO CommentDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CommentMapper extends EntityMapper<CommentDTO, Comment> {


}
