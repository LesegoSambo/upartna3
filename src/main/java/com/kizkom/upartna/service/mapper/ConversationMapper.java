package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.*;
import com.kizkom.upartna.service.dto.ConversationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Conversation and its DTO ConversationDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ConversationMapper extends EntityMapper<ConversationDTO, Conversation> {


}
