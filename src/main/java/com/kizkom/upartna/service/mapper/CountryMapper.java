package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.*;
import com.kizkom.upartna.service.dto.CountryDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Country and its DTO CountryDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CountryMapper extends EntityMapper<CountryDTO, Country> {


}
