package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.*;
import com.kizkom.upartna.service.dto.ExpertDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Expert and its DTO ExpertDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ExpertMapper extends EntityMapper<ExpertDTO, Expert> {


}
