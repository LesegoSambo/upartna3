package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.*;
import com.kizkom.upartna.service.dto.JobFileDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity JobFile and its DTO JobFileDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface JobFileMapper extends EntityMapper<JobFileDTO, JobFile> {


}
