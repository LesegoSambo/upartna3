package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.*;
import com.kizkom.upartna.service.dto.NotificationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Notification and its DTO NotificationDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface NotificationMapper extends EntityMapper<NotificationDTO, Notification> {


}
