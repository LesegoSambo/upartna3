package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.*;
import com.kizkom.upartna.service.dto.ProofFileDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ProofFile and its DTO ProofFileDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ProofFileMapper extends EntityMapper<ProofFileDTO, ProofFile> {


}
