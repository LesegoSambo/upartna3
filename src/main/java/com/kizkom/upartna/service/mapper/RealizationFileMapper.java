package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.*;
import com.kizkom.upartna.service.dto.RealizationFileDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity RealizationFile and its DTO RealizationFileDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RealizationFileMapper extends EntityMapper<RealizationFileDTO, RealizationFile> {


}
