package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.*;
import com.kizkom.upartna.service.dto.RealizationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Realization and its DTO RealizationDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RealizationMapper extends EntityMapper<RealizationDTO, Realization> {


}
