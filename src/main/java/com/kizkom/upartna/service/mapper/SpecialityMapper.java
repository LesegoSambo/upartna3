package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.*;
import com.kizkom.upartna.service.dto.SpecialityDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Speciality and its DTO SpecialityDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SpecialityMapper extends EntityMapper<SpecialityDTO, Speciality> {


}
