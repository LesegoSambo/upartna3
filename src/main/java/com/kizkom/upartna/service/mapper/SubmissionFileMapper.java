package com.kizkom.upartna.service.mapper;

import com.kizkom.upartna.domain.*;
import com.kizkom.upartna.service.dto.SubmissionFileDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SubmissionFile and its DTO SubmissionFileDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SubmissionFileMapper extends EntityMapper<SubmissionFileDTO, SubmissionFile> {


}
