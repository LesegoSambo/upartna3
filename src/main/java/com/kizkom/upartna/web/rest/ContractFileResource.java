package com.kizkom.upartna.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kizkom.upartna.service.ContractFileService;
import com.kizkom.upartna.web.rest.errors.BadRequestAlertException;
import com.kizkom.upartna.web.rest.util.HeaderUtil;
import com.kizkom.upartna.web.rest.util.PaginationUtil;
import com.kizkom.upartna.service.dto.ContractFileDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ContractFile.
 */
@RestController
@RequestMapping("/api")
public class ContractFileResource {

    private static final String ENTITY_NAME = "contractFile";
    private final Logger log = LoggerFactory.getLogger(ContractFileResource.class);
    private final ContractFileService contractFileService;

    public ContractFileResource(ContractFileService contractFileService) {
        this.contractFileService = contractFileService;
    }

    /**
     * POST  /contract-files : Create a new contractFile.
     *
     * @param contractFileDTO the contractFileDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new contractFileDTO, or with status 400 (Bad Request) if the contractFile has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/contract-files")
    @Timed
    public ResponseEntity<ContractFileDTO> createContractFile(@Valid @RequestBody ContractFileDTO contractFileDTO) throws URISyntaxException {
        log.debug("REST request to save ContractFile : {}", contractFileDTO);
        if (contractFileDTO.getId() != null) {
            throw new BadRequestAlertException("A new contractFile cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ContractFileDTO result = contractFileService.save(contractFileDTO);
        return ResponseEntity.created(new URI("/api/contract-files/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /contract-files : Updates an existing contractFile.
     *
     * @param contractFileDTO the contractFileDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated contractFileDTO,
     * or with status 400 (Bad Request) if the contractFileDTO is not valid,
     * or with status 500 (Internal Server Error) if the contractFileDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/contract-files")
    @Timed
    public ResponseEntity<ContractFileDTO> updateContractFile(@Valid @RequestBody ContractFileDTO contractFileDTO) throws URISyntaxException {
        log.debug("REST request to update ContractFile : {}", contractFileDTO);
        if (contractFileDTO.getId() == null) {
            return createContractFile(contractFileDTO);
        }
        ContractFileDTO result = contractFileService.save(contractFileDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, contractFileDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /contract-files : get all the contractFiles.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of contractFiles in body
     */
    @GetMapping("/contract-files")
    @Timed
    public ResponseEntity<List<ContractFileDTO>> getAllContractFiles(Pageable pageable) {
        log.debug("REST request to get a page of ContractFiles");
        Page<ContractFileDTO> page = contractFileService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/contract-files");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /contract-files/:id : get the "id" contractFile.
     *
     * @param id the id of the contractFileDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the contractFileDTO, or with status 404 (Not Found)
     */
    @GetMapping("/contract-files/{id}")
    @Timed
    public ResponseEntity<ContractFileDTO> getContractFile(@PathVariable String id) {
        log.debug("REST request to get ContractFile : {}", id);
        ContractFileDTO contractFileDTO = contractFileService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(contractFileDTO));
    }

    /**
     * DELETE  /contract-files/:id : delete the "id" contractFile.
     *
     * @param id the id of the contractFileDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/contract-files/{id}")
    @Timed
    public ResponseEntity<Void> deleteContractFile(@PathVariable String id) {
        log.debug("REST request to delete ContractFile : {}", id);
        contractFileService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }
}
