package com.kizkom.upartna.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kizkom.upartna.service.JobFileService;
import com.kizkom.upartna.web.rest.errors.BadRequestAlertException;
import com.kizkom.upartna.web.rest.util.HeaderUtil;
import com.kizkom.upartna.web.rest.util.PaginationUtil;
import com.kizkom.upartna.service.dto.JobFileDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing JobFile.
 */
@RestController
@RequestMapping("/api")
public class JobFileResource {

    private static final String ENTITY_NAME = "jobFile";
    private final Logger log = LoggerFactory.getLogger(JobFileResource.class);
    private final JobFileService jobFileService;

    public JobFileResource(JobFileService jobFileService) {
        this.jobFileService = jobFileService;
    }

    /**
     * POST  /job-files : Create a new jobFile.
     *
     * @param jobFileDTO the jobFileDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new jobFileDTO, or with status 400 (Bad Request) if the jobFile has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/job-files")
    @Timed
    public ResponseEntity<JobFileDTO> createJobFile(@RequestBody JobFileDTO jobFileDTO) throws URISyntaxException {
        log.debug("REST request to save JobFile : {}", jobFileDTO);
        if (jobFileDTO.getId() != null) {
            throw new BadRequestAlertException("A new jobFile cannot already have an ID", ENTITY_NAME, "idexists");
        }
        JobFileDTO result = jobFileService.save(jobFileDTO);
        return ResponseEntity.created(new URI("/api/job-files/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /job-files : Updates an existing jobFile.
     *
     * @param jobFileDTO the jobFileDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated jobFileDTO,
     * or with status 400 (Bad Request) if the jobFileDTO is not valid,
     * or with status 500 (Internal Server Error) if the jobFileDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/job-files")
    @Timed
    public ResponseEntity<JobFileDTO> updateJobFile(@RequestBody JobFileDTO jobFileDTO) throws URISyntaxException {
        log.debug("REST request to update JobFile : {}", jobFileDTO);
        if (jobFileDTO.getId() == null) {
            return createJobFile(jobFileDTO);
        }
        JobFileDTO result = jobFileService.save(jobFileDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, jobFileDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /job-files : get all the jobFiles.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of jobFiles in body
     */
    @GetMapping("/job-files")
    @Timed
    public ResponseEntity<List<JobFileDTO>> getAllJobFiles(Pageable pageable) {
        log.debug("REST request to get a page of JobFiles");
        Page<JobFileDTO> page = jobFileService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/job-files");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /job-files/:id : get the "id" jobFile.
     *
     * @param id the id of the jobFileDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the jobFileDTO, or with status 404 (Not Found)
     */
    @GetMapping("/job-files/{id}")
    @Timed
    public ResponseEntity<JobFileDTO> getJobFile(@PathVariable String id) {
        log.debug("REST request to get JobFile : {}", id);
        JobFileDTO jobFileDTO = jobFileService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(jobFileDTO));
    }

    /**
     * DELETE  /job-files/:id : delete the "id" jobFile.
     *
     * @param id the id of the jobFileDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/job-files/{id}")
    @Timed
    public ResponseEntity<Void> deleteJobFile(@PathVariable String id) {
        log.debug("REST request to delete JobFile : {}", id);
        jobFileService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }
}
