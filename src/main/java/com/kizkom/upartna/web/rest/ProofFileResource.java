package com.kizkom.upartna.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kizkom.upartna.service.ProofFileService;
import com.kizkom.upartna.web.rest.errors.BadRequestAlertException;
import com.kizkom.upartna.web.rest.util.HeaderUtil;
import com.kizkom.upartna.web.rest.util.PaginationUtil;
import com.kizkom.upartna.service.dto.ProofFileDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ProofFile.
 */
@RestController
@RequestMapping("/api")
public class ProofFileResource {

    private static final String ENTITY_NAME = "proofFile";
    private final Logger log = LoggerFactory.getLogger(ProofFileResource.class);
    private final ProofFileService proofFileService;

    public ProofFileResource(ProofFileService proofFileService) {
        this.proofFileService = proofFileService;
    }

    /**
     * POST  /proof-files : Create a new proofFile.
     *
     * @param proofFileDTO the proofFileDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new proofFileDTO, or with status 400 (Bad Request) if the proofFile has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/proof-files")
    @Timed
    public ResponseEntity<ProofFileDTO> createProofFile(@RequestBody ProofFileDTO proofFileDTO) throws URISyntaxException {
        log.debug("REST request to save ProofFile : {}", proofFileDTO);
        if (proofFileDTO.getId() != null) {
            throw new BadRequestAlertException("A new proofFile cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProofFileDTO result = proofFileService.save(proofFileDTO);
        return ResponseEntity.created(new URI("/api/proof-files/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /proof-files : Updates an existing proofFile.
     *
     * @param proofFileDTO the proofFileDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated proofFileDTO,
     * or with status 400 (Bad Request) if the proofFileDTO is not valid,
     * or with status 500 (Internal Server Error) if the proofFileDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/proof-files")
    @Timed
    public ResponseEntity<ProofFileDTO> updateProofFile(@RequestBody ProofFileDTO proofFileDTO) throws URISyntaxException {
        log.debug("REST request to update ProofFile : {}", proofFileDTO);
        if (proofFileDTO.getId() == null) {
            return createProofFile(proofFileDTO);
        }
        ProofFileDTO result = proofFileService.save(proofFileDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, proofFileDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /proof-files : get all the proofFiles.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of proofFiles in body
     */
    @GetMapping("/proof-files")
    @Timed
    public ResponseEntity<List<ProofFileDTO>> getAllProofFiles(Pageable pageable) {
        log.debug("REST request to get a page of ProofFiles");
        Page<ProofFileDTO> page = proofFileService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/proof-files");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /proof-files/:id : get the "id" proofFile.
     *
     * @param id the id of the proofFileDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the proofFileDTO, or with status 404 (Not Found)
     */
    @GetMapping("/proof-files/{id}")
    @Timed
    public ResponseEntity<ProofFileDTO> getProofFile(@PathVariable String id) {
        log.debug("REST request to get ProofFile : {}", id);
        ProofFileDTO proofFileDTO = proofFileService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(proofFileDTO));
    }

    /**
     * DELETE  /proof-files/:id : delete the "id" proofFile.
     *
     * @param id the id of the proofFileDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/proof-files/{id}")
    @Timed
    public ResponseEntity<Void> deleteProofFile(@PathVariable String id) {
        log.debug("REST request to delete ProofFile : {}", id);
        proofFileService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }
}
