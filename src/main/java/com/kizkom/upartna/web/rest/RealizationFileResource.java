package com.kizkom.upartna.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kizkom.upartna.service.RealizationFileService;
import com.kizkom.upartna.web.rest.errors.BadRequestAlertException;
import com.kizkom.upartna.web.rest.util.HeaderUtil;
import com.kizkom.upartna.web.rest.util.PaginationUtil;
import com.kizkom.upartna.service.dto.RealizationFileDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing RealizationFile.
 */
@RestController
@RequestMapping("/api")
public class RealizationFileResource {

    private static final String ENTITY_NAME = "realizationFile";
    private final Logger log = LoggerFactory.getLogger(RealizationFileResource.class);
    private final RealizationFileService realizationFileService;

    public RealizationFileResource(RealizationFileService realizationFileService) {
        this.realizationFileService = realizationFileService;
    }

    /**
     * POST  /realization-files : Create a new realizationFile.
     *
     * @param realizationFileDTO the realizationFileDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new realizationFileDTO, or with status 400 (Bad Request) if the realizationFile has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/realization-files")
    @Timed
    public ResponseEntity<RealizationFileDTO> createRealizationFile(@RequestBody RealizationFileDTO realizationFileDTO) throws URISyntaxException {
        log.debug("REST request to save RealizationFile : {}", realizationFileDTO);
        if (realizationFileDTO.getId() != null) {
            throw new BadRequestAlertException("A new realizationFile cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RealizationFileDTO result = realizationFileService.save(realizationFileDTO);
        return ResponseEntity.created(new URI("/api/realization-files/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /realization-files : Updates an existing realizationFile.
     *
     * @param realizationFileDTO the realizationFileDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated realizationFileDTO,
     * or with status 400 (Bad Request) if the realizationFileDTO is not valid,
     * or with status 500 (Internal Server Error) if the realizationFileDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/realization-files")
    @Timed
    public ResponseEntity<RealizationFileDTO> updateRealizationFile(@RequestBody RealizationFileDTO realizationFileDTO) throws URISyntaxException {
        log.debug("REST request to update RealizationFile : {}", realizationFileDTO);
        if (realizationFileDTO.getId() == null) {
            return createRealizationFile(realizationFileDTO);
        }
        RealizationFileDTO result = realizationFileService.save(realizationFileDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, realizationFileDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /realization-files : get all the realizationFiles.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of realizationFiles in body
     */
    @GetMapping("/realization-files")
    @Timed
    public ResponseEntity<List<RealizationFileDTO>> getAllRealizationFiles(Pageable pageable) {
        log.debug("REST request to get a page of RealizationFiles");
        Page<RealizationFileDTO> page = realizationFileService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/realization-files");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /realization-files/:id : get the "id" realizationFile.
     *
     * @param id the id of the realizationFileDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the realizationFileDTO, or with status 404 (Not Found)
     */
    @GetMapping("/realization-files/{id}")
    @Timed
    public ResponseEntity<RealizationFileDTO> getRealizationFile(@PathVariable String id) {
        log.debug("REST request to get RealizationFile : {}", id);
        RealizationFileDTO realizationFileDTO = realizationFileService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(realizationFileDTO));
    }

    /**
     * DELETE  /realization-files/:id : delete the "id" realizationFile.
     *
     * @param id the id of the realizationFileDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/realization-files/{id}")
    @Timed
    public ResponseEntity<Void> deleteRealizationFile(@PathVariable String id) {
        log.debug("REST request to delete RealizationFile : {}", id);
        realizationFileService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }
}
