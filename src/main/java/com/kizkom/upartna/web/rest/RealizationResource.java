package com.kizkom.upartna.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kizkom.upartna.service.RealizationService;
import com.kizkom.upartna.web.rest.errors.BadRequestAlertException;
import com.kizkom.upartna.web.rest.util.HeaderUtil;
import com.kizkom.upartna.web.rest.util.PaginationUtil;
import com.kizkom.upartna.service.dto.RealizationDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Realization.
 */
@RestController
@RequestMapping("/api")
public class RealizationResource {

    private static final String ENTITY_NAME = "realization";
    private final Logger log = LoggerFactory.getLogger(RealizationResource.class);
    private final RealizationService realizationService;

    public RealizationResource(RealizationService realizationService) {
        this.realizationService = realizationService;
    }

    /**
     * POST  /realizations : Create a new realization.
     *
     * @param realizationDTO the realizationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new realizationDTO, or with status 400 (Bad Request) if the realization has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/realizations")
    @Timed
    public ResponseEntity<RealizationDTO> createRealization(@Valid @RequestBody RealizationDTO realizationDTO) throws URISyntaxException {
        log.debug("REST request to save Realization : {}", realizationDTO);
        if (realizationDTO.getId() != null) {
            throw new BadRequestAlertException("A new realization cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RealizationDTO result = realizationService.save(realizationDTO);
        return ResponseEntity.created(new URI("/api/realizations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /realizations : Updates an existing realization.
     *
     * @param realizationDTO the realizationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated realizationDTO,
     * or with status 400 (Bad Request) if the realizationDTO is not valid,
     * or with status 500 (Internal Server Error) if the realizationDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/realizations")
    @Timed
    public ResponseEntity<RealizationDTO> updateRealization(@Valid @RequestBody RealizationDTO realizationDTO) throws URISyntaxException {
        log.debug("REST request to update Realization : {}", realizationDTO);
        if (realizationDTO.getId() == null) {
            return createRealization(realizationDTO);
        }
        RealizationDTO result = realizationService.save(realizationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, realizationDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /realizations : get all the realizations.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of realizations in body
     */
    @GetMapping("/realizations")
    @Timed
    public ResponseEntity<List<RealizationDTO>> getAllRealizations(Pageable pageable) {
        log.debug("REST request to get a page of Realizations");
        Page<RealizationDTO> page = realizationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/realizations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /realizations/:id : get the "id" realization.
     *
     * @param id the id of the realizationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the realizationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/realizations/{id}")
    @Timed
    public ResponseEntity<RealizationDTO> getRealization(@PathVariable String id) {
        log.debug("REST request to get Realization : {}", id);
        RealizationDTO realizationDTO = realizationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(realizationDTO));
    }

    /**
     * DELETE  /realizations/:id : delete the "id" realization.
     *
     * @param id the id of the realizationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/realizations/{id}")
    @Timed
    public ResponseEntity<Void> deleteRealization(@PathVariable String id) {
        log.debug("REST request to delete Realization : {}", id);
        realizationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }
}
