package com.kizkom.upartna.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kizkom.upartna.service.SubmissionFileService;
import com.kizkom.upartna.web.rest.errors.BadRequestAlertException;
import com.kizkom.upartna.web.rest.util.HeaderUtil;
import com.kizkom.upartna.web.rest.util.PaginationUtil;
import com.kizkom.upartna.service.dto.SubmissionFileDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SubmissionFile.
 */
@RestController
@RequestMapping("/api")
public class SubmissionFileResource {

    private static final String ENTITY_NAME = "submissionFile";
    private final Logger log = LoggerFactory.getLogger(SubmissionFileResource.class);
    private final SubmissionFileService submissionFileService;

    public SubmissionFileResource(SubmissionFileService submissionFileService) {
        this.submissionFileService = submissionFileService;
    }

    /**
     * POST  /submission-files : Create a new submissionFile.
     *
     * @param submissionFileDTO the submissionFileDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new submissionFileDTO, or with status 400 (Bad Request) if the submissionFile has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/submission-files")
    @Timed
    public ResponseEntity<SubmissionFileDTO> createSubmissionFile(@RequestBody SubmissionFileDTO submissionFileDTO) throws URISyntaxException {
        log.debug("REST request to save SubmissionFile : {}", submissionFileDTO);
        if (submissionFileDTO.getId() != null) {
            throw new BadRequestAlertException("A new submissionFile cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SubmissionFileDTO result = submissionFileService.save(submissionFileDTO);
        return ResponseEntity.created(new URI("/api/submission-files/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /submission-files : Updates an existing submissionFile.
     *
     * @param submissionFileDTO the submissionFileDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated submissionFileDTO,
     * or with status 400 (Bad Request) if the submissionFileDTO is not valid,
     * or with status 500 (Internal Server Error) if the submissionFileDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/submission-files")
    @Timed
    public ResponseEntity<SubmissionFileDTO> updateSubmissionFile(@RequestBody SubmissionFileDTO submissionFileDTO) throws URISyntaxException {
        log.debug("REST request to update SubmissionFile : {}", submissionFileDTO);
        if (submissionFileDTO.getId() == null) {
            return createSubmissionFile(submissionFileDTO);
        }
        SubmissionFileDTO result = submissionFileService.save(submissionFileDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, submissionFileDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /submission-files : get all the submissionFiles.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of submissionFiles in body
     */
    @GetMapping("/submission-files")
    @Timed
    public ResponseEntity<List<SubmissionFileDTO>> getAllSubmissionFiles(Pageable pageable) {
        log.debug("REST request to get a page of SubmissionFiles");
        Page<SubmissionFileDTO> page = submissionFileService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/submission-files");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /submission-files/:id : get the "id" submissionFile.
     *
     * @param id the id of the submissionFileDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the submissionFileDTO, or with status 404 (Not Found)
     */
    @GetMapping("/submission-files/{id}")
    @Timed
    public ResponseEntity<SubmissionFileDTO> getSubmissionFile(@PathVariable String id) {
        log.debug("REST request to get SubmissionFile : {}", id);
        SubmissionFileDTO submissionFileDTO = submissionFileService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(submissionFileDTO));
    }

    /**
     * DELETE  /submission-files/:id : delete the "id" submissionFile.
     *
     * @param id the id of the submissionFileDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/submission-files/{id}")
    @Timed
    public ResponseEntity<Void> deleteSubmissionFile(@PathVariable String id) {
        log.debug("REST request to delete SubmissionFile : {}", id);
        submissionFileService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }
}
