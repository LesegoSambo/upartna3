/**
 * View Models used by Spring MVC REST controllers.
 */
package com.kizkom.upartna.web.rest.vm;
