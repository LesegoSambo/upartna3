(function() {
    'use strict';

    angular
        .module('upartnaApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
