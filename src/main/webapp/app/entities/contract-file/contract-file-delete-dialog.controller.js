(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .controller('ContractFileDeleteController', ContractFileDeleteController);

    ContractFileDeleteController.$inject = ['$uibModalInstance', 'entity', 'ContractFile'];

    function ContractFileDeleteController($uibModalInstance, entity, ContractFile) {
        var vm = this;

        vm.contractFile = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete(id) {
            ContractFile.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
