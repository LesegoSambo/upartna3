(function () {
    'use strict';
    angular
        .module('upartnaApp')
        .factory('Contract', Contract);

    Contract.$inject = ['$resource', 'DateUtils'];

    function Contract($resource, DateUtils) {
        var resourceUrl = 'api/contracts/:id';

        return $resource(resourceUrl, {}, {
            'query': {method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.beginningDate = DateUtils.convertLocalDateFromServer(data.beginningDate);
                        data.endingDate = DateUtils.convertLocalDateFromServer(data.endingDate);
                        data.signatureDate = DateUtils.convertLocalDateFromServer(data.signatureDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.beginningDate = DateUtils.convertLocalDateToServer(copy.beginningDate);
                    copy.endingDate = DateUtils.convertLocalDateToServer(copy.endingDate);
                    copy.signatureDate = DateUtils.convertLocalDateToServer(copy.signatureDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.beginningDate = DateUtils.convertLocalDateToServer(copy.beginningDate);
                    copy.endingDate = DateUtils.convertLocalDateToServer(copy.endingDate);
                    copy.signatureDate = DateUtils.convertLocalDateToServer(copy.signatureDate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
