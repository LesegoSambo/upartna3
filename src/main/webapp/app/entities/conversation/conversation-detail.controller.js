(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .controller('ConversationDetailController', ConversationDetailController);

    ConversationDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Conversation'];

    function ConversationDetailController($scope, $rootScope, $stateParams, previousState, entity, Conversation) {
        var vm = this;

        vm.conversation = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('upartnaApp:conversationUpdate', function (event, result) {
            vm.conversation = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
