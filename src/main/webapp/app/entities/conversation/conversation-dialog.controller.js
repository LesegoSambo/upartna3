(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .controller('ConversationDialogController', ConversationDialogController);

    ConversationDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Conversation'];

    function ConversationDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, Conversation) {
        var vm = this;

        vm.conversation = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.conversation.id !== null) {
                Conversation.update(vm.conversation, onSaveSuccess, onSaveError);
            } else {
                Conversation.save(vm.conversation, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $scope.$emit('upartnaApp:conversationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.createdOn = false;
        vm.datePickerOpenStatus.closedOn = false;

        function openCalendar(date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
