(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .controller('ExpertDialogController', ExpertDialogController);

    ExpertDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Expert'];

    function ExpertDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, Expert) {
        var vm = this;

        vm.expert = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.expert.id !== null) {
                Expert.update(vm.expert, onSaveSuccess, onSaveError);
            } else {
                Expert.save(vm.expert, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $scope.$emit('upartnaApp:expertUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.experienceStartDate = false;
        vm.datePickerOpenStatus.activatedOn = false;
        vm.datePickerOpenStatus.createdOn = false;

        function openCalendar(date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
