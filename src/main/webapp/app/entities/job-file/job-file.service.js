(function () {
    'use strict';
    angular
        .module('upartnaApp')
        .factory('JobFile', JobFile);

    JobFile.$inject = ['$resource'];

    function JobFile($resource) {
        var resourceUrl = 'api/job-files/:id';

        return $resource(resourceUrl, {}, {
            'query': {method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': {method: 'PUT'}
        });
    }
})();
