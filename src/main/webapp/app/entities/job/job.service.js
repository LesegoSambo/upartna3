(function () {
    'use strict';
    angular
        .module('upartnaApp')
        .factory('Job', Job);

    Job.$inject = ['$resource', 'DateUtils'];

    function Job($resource, DateUtils) {
        var resourceUrl = 'api/jobs/:id';

        return $resource(resourceUrl, {}, {
            'query': {method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.beginningDate = DateUtils.convertLocalDateFromServer(data.beginningDate);
                        data.endingDate = DateUtils.convertLocalDateFromServer(data.endingDate);
                        data.createdOn = DateUtils.convertLocalDateFromServer(data.createdOn);
                        data.updatedOn = DateUtils.convertLocalDateFromServer(data.updatedOn);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.beginningDate = DateUtils.convertLocalDateToServer(copy.beginningDate);
                    copy.endingDate = DateUtils.convertLocalDateToServer(copy.endingDate);
                    copy.createdOn = DateUtils.convertLocalDateToServer(copy.createdOn);
                    copy.updatedOn = DateUtils.convertLocalDateToServer(copy.updatedOn);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.beginningDate = DateUtils.convertLocalDateToServer(copy.beginningDate);
                    copy.endingDate = DateUtils.convertLocalDateToServer(copy.endingDate);
                    copy.createdOn = DateUtils.convertLocalDateToServer(copy.createdOn);
                    copy.updatedOn = DateUtils.convertLocalDateToServer(copy.updatedOn);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
