(function () {
    'use strict';
    angular
        .module('upartnaApp')
        .factory('Message', Message);

    Message.$inject = ['$resource', 'DateUtils'];

    function Message($resource, DateUtils) {
        var resourceUrl = 'api/messages/:id';

        return $resource(resourceUrl, {}, {
            'query': {method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.sendingDate = DateUtils.convertLocalDateFromServer(data.sendingDate);
                        data.receivingDate = DateUtils.convertLocalDateFromServer(data.receivingDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.sendingDate = DateUtils.convertLocalDateToServer(copy.sendingDate);
                    copy.receivingDate = DateUtils.convertLocalDateToServer(copy.receivingDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.sendingDate = DateUtils.convertLocalDateToServer(copy.sendingDate);
                    copy.receivingDate = DateUtils.convertLocalDateToServer(copy.receivingDate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
