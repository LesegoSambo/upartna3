(function () {
    'use strict';
    angular
        .module('upartnaApp')
        .factory('Notification', Notification);

    Notification.$inject = ['$resource', 'DateUtils'];

    function Notification($resource, DateUtils) {
        var resourceUrl = 'api/notifications/:id';

        return $resource(resourceUrl, {}, {
            'query': {method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.sendingDate = DateUtils.convertLocalDateFromServer(data.sendingDate);
                        data.receivingDate = DateUtils.convertLocalDateFromServer(data.receivingDate);
                        data.viewDate = DateUtils.convertLocalDateFromServer(data.viewDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.sendingDate = DateUtils.convertLocalDateToServer(copy.sendingDate);
                    copy.receivingDate = DateUtils.convertLocalDateToServer(copy.receivingDate);
                    copy.viewDate = DateUtils.convertLocalDateToServer(copy.viewDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.sendingDate = DateUtils.convertLocalDateToServer(copy.sendingDate);
                    copy.receivingDate = DateUtils.convertLocalDateToServer(copy.receivingDate);
                    copy.viewDate = DateUtils.convertLocalDateToServer(copy.viewDate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
