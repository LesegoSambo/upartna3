(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .controller('ProofFileDeleteController', ProofFileDeleteController);

    ProofFileDeleteController.$inject = ['$uibModalInstance', 'entity', 'ProofFile'];

    function ProofFileDeleteController($uibModalInstance, entity, ProofFile) {
        var vm = this;

        vm.proofFile = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete(id) {
            ProofFile.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
