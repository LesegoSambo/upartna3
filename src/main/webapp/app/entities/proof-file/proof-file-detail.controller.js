(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .controller('ProofFileDetailController', ProofFileDetailController);

    ProofFileDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ProofFile'];

    function ProofFileDetailController($scope, $rootScope, $stateParams, previousState, entity, ProofFile) {
        var vm = this;

        vm.proofFile = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('upartnaApp:proofFileUpdate', function (event, result) {
            vm.proofFile = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
