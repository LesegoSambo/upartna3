(function () {
    'use strict';
    angular
        .module('upartnaApp')
        .factory('ProofFile', ProofFile);

    ProofFile.$inject = ['$resource'];

    function ProofFile($resource) {
        var resourceUrl = 'api/proof-files/:id';

        return $resource(resourceUrl, {}, {
            'query': {method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': {method: 'PUT'}
        });
    }
})();
