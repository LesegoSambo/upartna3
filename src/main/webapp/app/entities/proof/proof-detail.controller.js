(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .controller('ProofDetailController', ProofDetailController);

    ProofDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Proof'];

    function ProofDetailController($scope, $rootScope, $stateParams, previousState, entity, Proof) {
        var vm = this;

        vm.proof = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('upartnaApp:proofUpdate', function (event, result) {
            vm.proof = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
