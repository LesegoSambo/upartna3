(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .controller('ProofDialogController', ProofDialogController);

    ProofDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Proof'];

    function ProofDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, Proof) {
        var vm = this;

        vm.proof = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.proof.id !== null) {
                Proof.update(vm.proof, onSaveSuccess, onSaveError);
            } else {
                Proof.save(vm.proof, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $scope.$emit('upartnaApp:proofUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.createdOn = false;

        function openCalendar(date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
