(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .controller('RealizationDialogController', RealizationDialogController);

    RealizationDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Realization'];

    function RealizationDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, Realization) {
        var vm = this;

        vm.realization = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.realization.id !== null) {
                Realization.update(vm.realization, onSaveSuccess, onSaveError);
            } else {
                Realization.save(vm.realization, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $scope.$emit('upartnaApp:realizationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.realizationDate = false;

        function openCalendar(date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
