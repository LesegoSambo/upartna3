(function () {
    'use strict';

    angular
        .module('upartnaApp')
        .controller('SubmissionFileDialogController', SubmissionFileDialogController);

    SubmissionFileDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'SubmissionFile'];

    function SubmissionFileDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, SubmissionFile) {
        var vm = this;

        vm.submissionFile = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.submissionFile.id !== null) {
                SubmissionFile.update(vm.submissionFile, onSaveSuccess, onSaveError);
            } else {
                SubmissionFile.save(vm.submissionFile, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $scope.$emit('upartnaApp:submissionFileUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }


    }
})();
