(function () {
    'use strict';
    angular
        .module('upartnaApp')
        .factory('SubmissionFile', SubmissionFile);

    SubmissionFile.$inject = ['$resource'];

    function SubmissionFile($resource) {
        var resourceUrl = 'api/submission-files/:id';

        return $resource(resourceUrl, {}, {
            'query': {method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': {method: 'PUT'}
        });
    }
})();
