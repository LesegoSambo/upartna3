package com.kizkom.upartna.web.rest;

import com.kizkom.upartna.UpartnaApp;

import com.kizkom.upartna.domain.ContractFile;
import com.kizkom.upartna.repository.ContractFileRepository;
import com.kizkom.upartna.service.ContractFileService;
import com.kizkom.upartna.service.dto.ContractFileDTO;
import com.kizkom.upartna.service.mapper.ContractFileMapper;
import com.kizkom.upartna.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static com.kizkom.upartna.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ContractFileResource REST controller.
 *
 * @see ContractFileResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = UpartnaApp.class)
public class ContractFileResourceIntTest {

    private static final String DEFAULT_URL = "AAAAAAAAAA";
    private static final String UPDATED_URL = "BBBBBBBBBB";

    private static final String DEFAULT_FILE_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_FILE_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_EXTENSION = "AAAAAAAAAA";
    private static final String UPDATED_EXTENSION = "BBBBBBBBBB";

    @Autowired
    private ContractFileRepository contractFileRepository;

    @Autowired
    private ContractFileMapper contractFileMapper;

    @Autowired
    private ContractFileService contractFileService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restContractFileMockMvc;

    private ContractFile contractFile;

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContractFile createEntity() {
        ContractFile contractFile = new ContractFile()
            .url(DEFAULT_URL)
            .fileType(DEFAULT_FILE_TYPE)
            .extension(DEFAULT_EXTENSION);
        return contractFile;
    }

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ContractFileResource contractFileResource = new ContractFileResource(contractFileService);
        this.restContractFileMockMvc = MockMvcBuilders.standaloneSetup(contractFileResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        contractFileRepository.deleteAll();
        contractFile = createEntity();
    }

    @Test
    public void createContractFile() throws Exception {
        int databaseSizeBeforeCreate = contractFileRepository.findAll().size();

        // Create the ContractFile
        ContractFileDTO contractFileDTO = contractFileMapper.toDto(contractFile);
        restContractFileMockMvc.perform(post("/api/contract-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contractFileDTO)))
            .andExpect(status().isCreated());

        // Validate the ContractFile in the database
        List<ContractFile> contractFileList = contractFileRepository.findAll();
        assertThat(contractFileList).hasSize(databaseSizeBeforeCreate + 1);
        ContractFile testContractFile = contractFileList.get(contractFileList.size() - 1);
        assertThat(testContractFile.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testContractFile.getFileType()).isEqualTo(DEFAULT_FILE_TYPE);
        assertThat(testContractFile.getExtension()).isEqualTo(DEFAULT_EXTENSION);
    }

    @Test
    public void createContractFileWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = contractFileRepository.findAll().size();

        // Create the ContractFile with an existing ID
        contractFile.setId("existing_id");
        ContractFileDTO contractFileDTO = contractFileMapper.toDto(contractFile);

        // An entity with an existing ID cannot be created, so this API call must fail
        restContractFileMockMvc.perform(post("/api/contract-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contractFileDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ContractFile in the database
        List<ContractFile> contractFileList = contractFileRepository.findAll();
        assertThat(contractFileList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void checkUrlIsRequired() throws Exception {
        int databaseSizeBeforeTest = contractFileRepository.findAll().size();
        // set the field null
        contractFile.setUrl(null);

        // Create the ContractFile, which fails.
        ContractFileDTO contractFileDTO = contractFileMapper.toDto(contractFile);

        restContractFileMockMvc.perform(post("/api/contract-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contractFileDTO)))
            .andExpect(status().isBadRequest());

        List<ContractFile> contractFileList = contractFileRepository.findAll();
        assertThat(contractFileList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllContractFiles() throws Exception {
        // Initialize the database
        contractFileRepository.save(contractFile);

        // Get all the contractFileList
        restContractFileMockMvc.perform(get("/api/contract-files?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contractFile.getId())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].fileType").value(hasItem(DEFAULT_FILE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].extension").value(hasItem(DEFAULT_EXTENSION.toString())));
    }

    @Test
    public void getContractFile() throws Exception {
        // Initialize the database
        contractFileRepository.save(contractFile);

        // Get the contractFile
        restContractFileMockMvc.perform(get("/api/contract-files/{id}", contractFile.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(contractFile.getId()))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()))
            .andExpect(jsonPath("$.fileType").value(DEFAULT_FILE_TYPE.toString()))
            .andExpect(jsonPath("$.extension").value(DEFAULT_EXTENSION.toString()));
    }

    @Test
    public void getNonExistingContractFile() throws Exception {
        // Get the contractFile
        restContractFileMockMvc.perform(get("/api/contract-files/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateContractFile() throws Exception {
        // Initialize the database
        contractFileRepository.save(contractFile);
        int databaseSizeBeforeUpdate = contractFileRepository.findAll().size();

        // Update the contractFile
        ContractFile updatedContractFile = contractFileRepository.findOne(contractFile.getId());
        updatedContractFile
            .url(UPDATED_URL)
            .fileType(UPDATED_FILE_TYPE)
            .extension(UPDATED_EXTENSION);
        ContractFileDTO contractFileDTO = contractFileMapper.toDto(updatedContractFile);

        restContractFileMockMvc.perform(put("/api/contract-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contractFileDTO)))
            .andExpect(status().isOk());

        // Validate the ContractFile in the database
        List<ContractFile> contractFileList = contractFileRepository.findAll();
        assertThat(contractFileList).hasSize(databaseSizeBeforeUpdate);
        ContractFile testContractFile = contractFileList.get(contractFileList.size() - 1);
        assertThat(testContractFile.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(testContractFile.getFileType()).isEqualTo(UPDATED_FILE_TYPE);
        assertThat(testContractFile.getExtension()).isEqualTo(UPDATED_EXTENSION);
    }

    @Test
    public void updateNonExistingContractFile() throws Exception {
        int databaseSizeBeforeUpdate = contractFileRepository.findAll().size();

        // Create the ContractFile
        ContractFileDTO contractFileDTO = contractFileMapper.toDto(contractFile);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restContractFileMockMvc.perform(put("/api/contract-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contractFileDTO)))
            .andExpect(status().isCreated());

        // Validate the ContractFile in the database
        List<ContractFile> contractFileList = contractFileRepository.findAll();
        assertThat(contractFileList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteContractFile() throws Exception {
        // Initialize the database
        contractFileRepository.save(contractFile);
        int databaseSizeBeforeDelete = contractFileRepository.findAll().size();

        // Get the contractFile
        restContractFileMockMvc.perform(delete("/api/contract-files/{id}", contractFile.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ContractFile> contractFileList = contractFileRepository.findAll();
        assertThat(contractFileList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContractFile.class);
        ContractFile contractFile1 = new ContractFile();
        contractFile1.setId("id1");
        ContractFile contractFile2 = new ContractFile();
        contractFile2.setId(contractFile1.getId());
        assertThat(contractFile1).isEqualTo(contractFile2);
        contractFile2.setId("id2");
        assertThat(contractFile1).isNotEqualTo(contractFile2);
        contractFile1.setId(null);
        assertThat(contractFile1).isNotEqualTo(contractFile2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContractFileDTO.class);
        ContractFileDTO contractFileDTO1 = new ContractFileDTO();
        contractFileDTO1.setId("id1");
        ContractFileDTO contractFileDTO2 = new ContractFileDTO();
        assertThat(contractFileDTO1).isNotEqualTo(contractFileDTO2);
        contractFileDTO2.setId(contractFileDTO1.getId());
        assertThat(contractFileDTO1).isEqualTo(contractFileDTO2);
        contractFileDTO2.setId("id2");
        assertThat(contractFileDTO1).isNotEqualTo(contractFileDTO2);
        contractFileDTO1.setId(null);
        assertThat(contractFileDTO1).isNotEqualTo(contractFileDTO2);
    }
}
