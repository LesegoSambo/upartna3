package com.kizkom.upartna.web.rest;

import com.kizkom.upartna.UpartnaApp;

import com.kizkom.upartna.domain.Expert;
import com.kizkom.upartna.repository.ExpertRepository;
import com.kizkom.upartna.service.ExpertService;
import com.kizkom.upartna.service.dto.ExpertDTO;
import com.kizkom.upartna.service.mapper.ExpertMapper;
import com.kizkom.upartna.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.kizkom.upartna.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ExpertResource REST controller.
 *
 * @see ExpertResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = UpartnaApp.class)
public class ExpertResourceIntTest {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_ABOUT = "AAAAAAAAAA";
    private static final String UPDATED_ABOUT = "BBBBBBBBBB";

    private static final String DEFAULT_WEBSITE = "AAAAAAAAAA";
    private static final String UPDATED_WEBSITE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_IS_ACTIVATED = false;
    private static final Boolean UPDATED_IS_ACTIVATED = true;

    private static final Boolean DEFAULT_IS_COMPANY = false;
    private static final Boolean UPDATED_IS_COMPANY = true;

    private static final String DEFAULT_TAX_PAYER_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_TAX_PAYER_NUMBER = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_EXPERIENCE_START_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_EXPERIENCE_START_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_ACTIVATED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ACTIVATED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_CREATED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_ON = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private ExpertRepository expertRepository;

    @Autowired
    private ExpertMapper expertMapper;

    @Autowired
    private ExpertService expertService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restExpertMockMvc;

    private Expert expert;

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Expert createEntity() {
        Expert expert = new Expert()
            .title(DEFAULT_TITLE)
            .about(DEFAULT_ABOUT)
            .website(DEFAULT_WEBSITE)
            .isActivated(DEFAULT_IS_ACTIVATED)
            .isCompany(DEFAULT_IS_COMPANY)
            .taxPayerNumber(DEFAULT_TAX_PAYER_NUMBER)
            .experienceStartDate(DEFAULT_EXPERIENCE_START_DATE)
            .activatedOn(DEFAULT_ACTIVATED_ON)
            .createdOn(DEFAULT_CREATED_ON);
        return expert;
    }

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ExpertResource expertResource = new ExpertResource(expertService);
        this.restExpertMockMvc = MockMvcBuilders.standaloneSetup(expertResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        expertRepository.deleteAll();
        expert = createEntity();
    }

    @Test
    public void createExpert() throws Exception {
        int databaseSizeBeforeCreate = expertRepository.findAll().size();

        // Create the Expert
        ExpertDTO expertDTO = expertMapper.toDto(expert);
        restExpertMockMvc.perform(post("/api/experts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(expertDTO)))
            .andExpect(status().isCreated());

        // Validate the Expert in the database
        List<Expert> expertList = expertRepository.findAll();
        assertThat(expertList).hasSize(databaseSizeBeforeCreate + 1);
        Expert testExpert = expertList.get(expertList.size() - 1);
        assertThat(testExpert.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testExpert.getAbout()).isEqualTo(DEFAULT_ABOUT);
        assertThat(testExpert.getWebsite()).isEqualTo(DEFAULT_WEBSITE);
        assertThat(testExpert.isIsActivated()).isEqualTo(DEFAULT_IS_ACTIVATED);
        assertThat(testExpert.isIsCompany()).isEqualTo(DEFAULT_IS_COMPANY);
        assertThat(testExpert.getTaxPayerNumber()).isEqualTo(DEFAULT_TAX_PAYER_NUMBER);
        assertThat(testExpert.getExperienceStartDate()).isEqualTo(DEFAULT_EXPERIENCE_START_DATE);
        assertThat(testExpert.getActivatedOn()).isEqualTo(DEFAULT_ACTIVATED_ON);
        assertThat(testExpert.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
    }

    @Test
    public void createExpertWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = expertRepository.findAll().size();

        // Create the Expert with an existing ID
        expert.setId("existing_id");
        ExpertDTO expertDTO = expertMapper.toDto(expert);

        // An entity with an existing ID cannot be created, so this API call must fail
        restExpertMockMvc.perform(post("/api/experts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(expertDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Expert in the database
        List<Expert> expertList = expertRepository.findAll();
        assertThat(expertList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllExperts() throws Exception {
        // Initialize the database
        expertRepository.save(expert);

        // Get all the expertList
        restExpertMockMvc.perform(get("/api/experts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(expert.getId())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].about").value(hasItem(DEFAULT_ABOUT.toString())))
            .andExpect(jsonPath("$.[*].website").value(hasItem(DEFAULT_WEBSITE.toString())))
            .andExpect(jsonPath("$.[*].isActivated").value(hasItem(DEFAULT_IS_ACTIVATED.booleanValue())))
            .andExpect(jsonPath("$.[*].isCompany").value(hasItem(DEFAULT_IS_COMPANY.booleanValue())))
            .andExpect(jsonPath("$.[*].taxPayerNumber").value(hasItem(DEFAULT_TAX_PAYER_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].experienceStartDate").value(hasItem(DEFAULT_EXPERIENCE_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].activatedOn").value(hasItem(DEFAULT_ACTIVATED_ON.toString())))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())));
    }

    @Test
    public void getExpert() throws Exception {
        // Initialize the database
        expertRepository.save(expert);

        // Get the expert
        restExpertMockMvc.perform(get("/api/experts/{id}", expert.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(expert.getId()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.about").value(DEFAULT_ABOUT.toString()))
            .andExpect(jsonPath("$.website").value(DEFAULT_WEBSITE.toString()))
            .andExpect(jsonPath("$.isActivated").value(DEFAULT_IS_ACTIVATED.booleanValue()))
            .andExpect(jsonPath("$.isCompany").value(DEFAULT_IS_COMPANY.booleanValue()))
            .andExpect(jsonPath("$.taxPayerNumber").value(DEFAULT_TAX_PAYER_NUMBER.toString()))
            .andExpect(jsonPath("$.experienceStartDate").value(DEFAULT_EXPERIENCE_START_DATE.toString()))
            .andExpect(jsonPath("$.activatedOn").value(DEFAULT_ACTIVATED_ON.toString()))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()));
    }

    @Test
    public void getNonExistingExpert() throws Exception {
        // Get the expert
        restExpertMockMvc.perform(get("/api/experts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateExpert() throws Exception {
        // Initialize the database
        expertRepository.save(expert);
        int databaseSizeBeforeUpdate = expertRepository.findAll().size();

        // Update the expert
        Expert updatedExpert = expertRepository.findOne(expert.getId());
        updatedExpert
            .title(UPDATED_TITLE)
            .about(UPDATED_ABOUT)
            .website(UPDATED_WEBSITE)
            .isActivated(UPDATED_IS_ACTIVATED)
            .isCompany(UPDATED_IS_COMPANY)
            .taxPayerNumber(UPDATED_TAX_PAYER_NUMBER)
            .experienceStartDate(UPDATED_EXPERIENCE_START_DATE)
            .activatedOn(UPDATED_ACTIVATED_ON)
            .createdOn(UPDATED_CREATED_ON);
        ExpertDTO expertDTO = expertMapper.toDto(updatedExpert);

        restExpertMockMvc.perform(put("/api/experts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(expertDTO)))
            .andExpect(status().isOk());

        // Validate the Expert in the database
        List<Expert> expertList = expertRepository.findAll();
        assertThat(expertList).hasSize(databaseSizeBeforeUpdate);
        Expert testExpert = expertList.get(expertList.size() - 1);
        assertThat(testExpert.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testExpert.getAbout()).isEqualTo(UPDATED_ABOUT);
        assertThat(testExpert.getWebsite()).isEqualTo(UPDATED_WEBSITE);
        assertThat(testExpert.isIsActivated()).isEqualTo(UPDATED_IS_ACTIVATED);
        assertThat(testExpert.isIsCompany()).isEqualTo(UPDATED_IS_COMPANY);
        assertThat(testExpert.getTaxPayerNumber()).isEqualTo(UPDATED_TAX_PAYER_NUMBER);
        assertThat(testExpert.getExperienceStartDate()).isEqualTo(UPDATED_EXPERIENCE_START_DATE);
        assertThat(testExpert.getActivatedOn()).isEqualTo(UPDATED_ACTIVATED_ON);
        assertThat(testExpert.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
    }

    @Test
    public void updateNonExistingExpert() throws Exception {
        int databaseSizeBeforeUpdate = expertRepository.findAll().size();

        // Create the Expert
        ExpertDTO expertDTO = expertMapper.toDto(expert);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restExpertMockMvc.perform(put("/api/experts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(expertDTO)))
            .andExpect(status().isCreated());

        // Validate the Expert in the database
        List<Expert> expertList = expertRepository.findAll();
        assertThat(expertList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteExpert() throws Exception {
        // Initialize the database
        expertRepository.save(expert);
        int databaseSizeBeforeDelete = expertRepository.findAll().size();

        // Get the expert
        restExpertMockMvc.perform(delete("/api/experts/{id}", expert.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Expert> expertList = expertRepository.findAll();
        assertThat(expertList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Expert.class);
        Expert expert1 = new Expert();
        expert1.setId("id1");
        Expert expert2 = new Expert();
        expert2.setId(expert1.getId());
        assertThat(expert1).isEqualTo(expert2);
        expert2.setId("id2");
        assertThat(expert1).isNotEqualTo(expert2);
        expert1.setId(null);
        assertThat(expert1).isNotEqualTo(expert2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExpertDTO.class);
        ExpertDTO expertDTO1 = new ExpertDTO();
        expertDTO1.setId("id1");
        ExpertDTO expertDTO2 = new ExpertDTO();
        assertThat(expertDTO1).isNotEqualTo(expertDTO2);
        expertDTO2.setId(expertDTO1.getId());
        assertThat(expertDTO1).isEqualTo(expertDTO2);
        expertDTO2.setId("id2");
        assertThat(expertDTO1).isNotEqualTo(expertDTO2);
        expertDTO1.setId(null);
        assertThat(expertDTO1).isNotEqualTo(expertDTO2);
    }
}
