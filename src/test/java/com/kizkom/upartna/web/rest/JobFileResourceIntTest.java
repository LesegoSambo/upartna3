package com.kizkom.upartna.web.rest;

import com.kizkom.upartna.UpartnaApp;

import com.kizkom.upartna.domain.JobFile;
import com.kizkom.upartna.repository.JobFileRepository;
import com.kizkom.upartna.service.JobFileService;
import com.kizkom.upartna.service.dto.JobFileDTO;
import com.kizkom.upartna.service.mapper.JobFileMapper;
import com.kizkom.upartna.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static com.kizkom.upartna.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the JobFileResource REST controller.
 *
 * @see JobFileResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = UpartnaApp.class)
public class JobFileResourceIntTest {

    private static final String DEFAULT_URL = "AAAAAAAAAA";
    private static final String UPDATED_URL = "BBBBBBBBBB";

    private static final String DEFAULT_FILE_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_FILE_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_EXTENSION = "AAAAAAAAAA";
    private static final String UPDATED_EXTENSION = "BBBBBBBBBB";

    @Autowired
    private JobFileRepository jobFileRepository;

    @Autowired
    private JobFileMapper jobFileMapper;

    @Autowired
    private JobFileService jobFileService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restJobFileMockMvc;

    private JobFile jobFile;

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static JobFile createEntity() {
        JobFile jobFile = new JobFile()
            .url(DEFAULT_URL)
            .fileType(DEFAULT_FILE_TYPE)
            .extension(DEFAULT_EXTENSION);
        return jobFile;
    }

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final JobFileResource jobFileResource = new JobFileResource(jobFileService);
        this.restJobFileMockMvc = MockMvcBuilders.standaloneSetup(jobFileResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        jobFileRepository.deleteAll();
        jobFile = createEntity();
    }

    @Test
    public void createJobFile() throws Exception {
        int databaseSizeBeforeCreate = jobFileRepository.findAll().size();

        // Create the JobFile
        JobFileDTO jobFileDTO = jobFileMapper.toDto(jobFile);
        restJobFileMockMvc.perform(post("/api/job-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobFileDTO)))
            .andExpect(status().isCreated());

        // Validate the JobFile in the database
        List<JobFile> jobFileList = jobFileRepository.findAll();
        assertThat(jobFileList).hasSize(databaseSizeBeforeCreate + 1);
        JobFile testJobFile = jobFileList.get(jobFileList.size() - 1);
        assertThat(testJobFile.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testJobFile.getFileType()).isEqualTo(DEFAULT_FILE_TYPE);
        assertThat(testJobFile.getExtension()).isEqualTo(DEFAULT_EXTENSION);
    }

    @Test
    public void createJobFileWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = jobFileRepository.findAll().size();

        // Create the JobFile with an existing ID
        jobFile.setId("existing_id");
        JobFileDTO jobFileDTO = jobFileMapper.toDto(jobFile);

        // An entity with an existing ID cannot be created, so this API call must fail
        restJobFileMockMvc.perform(post("/api/job-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobFileDTO)))
            .andExpect(status().isBadRequest());

        // Validate the JobFile in the database
        List<JobFile> jobFileList = jobFileRepository.findAll();
        assertThat(jobFileList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllJobFiles() throws Exception {
        // Initialize the database
        jobFileRepository.save(jobFile);

        // Get all the jobFileList
        restJobFileMockMvc.perform(get("/api/job-files?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(jobFile.getId())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].fileType").value(hasItem(DEFAULT_FILE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].extension").value(hasItem(DEFAULT_EXTENSION.toString())));
    }

    @Test
    public void getJobFile() throws Exception {
        // Initialize the database
        jobFileRepository.save(jobFile);

        // Get the jobFile
        restJobFileMockMvc.perform(get("/api/job-files/{id}", jobFile.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(jobFile.getId()))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()))
            .andExpect(jsonPath("$.fileType").value(DEFAULT_FILE_TYPE.toString()))
            .andExpect(jsonPath("$.extension").value(DEFAULT_EXTENSION.toString()));
    }

    @Test
    public void getNonExistingJobFile() throws Exception {
        // Get the jobFile
        restJobFileMockMvc.perform(get("/api/job-files/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateJobFile() throws Exception {
        // Initialize the database
        jobFileRepository.save(jobFile);
        int databaseSizeBeforeUpdate = jobFileRepository.findAll().size();

        // Update the jobFile
        JobFile updatedJobFile = jobFileRepository.findOne(jobFile.getId());
        updatedJobFile
            .url(UPDATED_URL)
            .fileType(UPDATED_FILE_TYPE)
            .extension(UPDATED_EXTENSION);
        JobFileDTO jobFileDTO = jobFileMapper.toDto(updatedJobFile);

        restJobFileMockMvc.perform(put("/api/job-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobFileDTO)))
            .andExpect(status().isOk());

        // Validate the JobFile in the database
        List<JobFile> jobFileList = jobFileRepository.findAll();
        assertThat(jobFileList).hasSize(databaseSizeBeforeUpdate);
        JobFile testJobFile = jobFileList.get(jobFileList.size() - 1);
        assertThat(testJobFile.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(testJobFile.getFileType()).isEqualTo(UPDATED_FILE_TYPE);
        assertThat(testJobFile.getExtension()).isEqualTo(UPDATED_EXTENSION);
    }

    @Test
    public void updateNonExistingJobFile() throws Exception {
        int databaseSizeBeforeUpdate = jobFileRepository.findAll().size();

        // Create the JobFile
        JobFileDTO jobFileDTO = jobFileMapper.toDto(jobFile);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restJobFileMockMvc.perform(put("/api/job-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobFileDTO)))
            .andExpect(status().isCreated());

        // Validate the JobFile in the database
        List<JobFile> jobFileList = jobFileRepository.findAll();
        assertThat(jobFileList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteJobFile() throws Exception {
        // Initialize the database
        jobFileRepository.save(jobFile);
        int databaseSizeBeforeDelete = jobFileRepository.findAll().size();

        // Get the jobFile
        restJobFileMockMvc.perform(delete("/api/job-files/{id}", jobFile.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<JobFile> jobFileList = jobFileRepository.findAll();
        assertThat(jobFileList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(JobFile.class);
        JobFile jobFile1 = new JobFile();
        jobFile1.setId("id1");
        JobFile jobFile2 = new JobFile();
        jobFile2.setId(jobFile1.getId());
        assertThat(jobFile1).isEqualTo(jobFile2);
        jobFile2.setId("id2");
        assertThat(jobFile1).isNotEqualTo(jobFile2);
        jobFile1.setId(null);
        assertThat(jobFile1).isNotEqualTo(jobFile2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(JobFileDTO.class);
        JobFileDTO jobFileDTO1 = new JobFileDTO();
        jobFileDTO1.setId("id1");
        JobFileDTO jobFileDTO2 = new JobFileDTO();
        assertThat(jobFileDTO1).isNotEqualTo(jobFileDTO2);
        jobFileDTO2.setId(jobFileDTO1.getId());
        assertThat(jobFileDTO1).isEqualTo(jobFileDTO2);
        jobFileDTO2.setId("id2");
        assertThat(jobFileDTO1).isNotEqualTo(jobFileDTO2);
        jobFileDTO1.setId(null);
        assertThat(jobFileDTO1).isNotEqualTo(jobFileDTO2);
    }
}
