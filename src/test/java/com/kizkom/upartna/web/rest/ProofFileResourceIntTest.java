package com.kizkom.upartna.web.rest;

import com.kizkom.upartna.UpartnaApp;

import com.kizkom.upartna.domain.ProofFile;
import com.kizkom.upartna.repository.ProofFileRepository;
import com.kizkom.upartna.service.ProofFileService;
import com.kizkom.upartna.service.dto.ProofFileDTO;
import com.kizkom.upartna.service.mapper.ProofFileMapper;
import com.kizkom.upartna.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static com.kizkom.upartna.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ProofFileResource REST controller.
 *
 * @see ProofFileResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = UpartnaApp.class)
public class ProofFileResourceIntTest {

    private static final String DEFAULT_URL = "AAAAAAAAAA";
    private static final String UPDATED_URL = "BBBBBBBBBB";

    private static final String DEFAULT_FILE_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_FILE_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_EXTENSION = "AAAAAAAAAA";
    private static final String UPDATED_EXTENSION = "BBBBBBBBBB";

    @Autowired
    private ProofFileRepository proofFileRepository;

    @Autowired
    private ProofFileMapper proofFileMapper;

    @Autowired
    private ProofFileService proofFileService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restProofFileMockMvc;

    private ProofFile proofFile;

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProofFile createEntity() {
        ProofFile proofFile = new ProofFile()
            .url(DEFAULT_URL)
            .fileType(DEFAULT_FILE_TYPE)
            .extension(DEFAULT_EXTENSION);
        return proofFile;
    }

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProofFileResource proofFileResource = new ProofFileResource(proofFileService);
        this.restProofFileMockMvc = MockMvcBuilders.standaloneSetup(proofFileResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        proofFileRepository.deleteAll();
        proofFile = createEntity();
    }

    @Test
    public void createProofFile() throws Exception {
        int databaseSizeBeforeCreate = proofFileRepository.findAll().size();

        // Create the ProofFile
        ProofFileDTO proofFileDTO = proofFileMapper.toDto(proofFile);
        restProofFileMockMvc.perform(post("/api/proof-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(proofFileDTO)))
            .andExpect(status().isCreated());

        // Validate the ProofFile in the database
        List<ProofFile> proofFileList = proofFileRepository.findAll();
        assertThat(proofFileList).hasSize(databaseSizeBeforeCreate + 1);
        ProofFile testProofFile = proofFileList.get(proofFileList.size() - 1);
        assertThat(testProofFile.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testProofFile.getFileType()).isEqualTo(DEFAULT_FILE_TYPE);
        assertThat(testProofFile.getExtension()).isEqualTo(DEFAULT_EXTENSION);
    }

    @Test
    public void createProofFileWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = proofFileRepository.findAll().size();

        // Create the ProofFile with an existing ID
        proofFile.setId("existing_id");
        ProofFileDTO proofFileDTO = proofFileMapper.toDto(proofFile);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProofFileMockMvc.perform(post("/api/proof-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(proofFileDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProofFile in the database
        List<ProofFile> proofFileList = proofFileRepository.findAll();
        assertThat(proofFileList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllProofFiles() throws Exception {
        // Initialize the database
        proofFileRepository.save(proofFile);

        // Get all the proofFileList
        restProofFileMockMvc.perform(get("/api/proof-files?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(proofFile.getId())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].fileType").value(hasItem(DEFAULT_FILE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].extension").value(hasItem(DEFAULT_EXTENSION.toString())));
    }

    @Test
    public void getProofFile() throws Exception {
        // Initialize the database
        proofFileRepository.save(proofFile);

        // Get the proofFile
        restProofFileMockMvc.perform(get("/api/proof-files/{id}", proofFile.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(proofFile.getId()))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()))
            .andExpect(jsonPath("$.fileType").value(DEFAULT_FILE_TYPE.toString()))
            .andExpect(jsonPath("$.extension").value(DEFAULT_EXTENSION.toString()));
    }

    @Test
    public void getNonExistingProofFile() throws Exception {
        // Get the proofFile
        restProofFileMockMvc.perform(get("/api/proof-files/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateProofFile() throws Exception {
        // Initialize the database
        proofFileRepository.save(proofFile);
        int databaseSizeBeforeUpdate = proofFileRepository.findAll().size();

        // Update the proofFile
        ProofFile updatedProofFile = proofFileRepository.findOne(proofFile.getId());
        updatedProofFile
            .url(UPDATED_URL)
            .fileType(UPDATED_FILE_TYPE)
            .extension(UPDATED_EXTENSION);
        ProofFileDTO proofFileDTO = proofFileMapper.toDto(updatedProofFile);

        restProofFileMockMvc.perform(put("/api/proof-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(proofFileDTO)))
            .andExpect(status().isOk());

        // Validate the ProofFile in the database
        List<ProofFile> proofFileList = proofFileRepository.findAll();
        assertThat(proofFileList).hasSize(databaseSizeBeforeUpdate);
        ProofFile testProofFile = proofFileList.get(proofFileList.size() - 1);
        assertThat(testProofFile.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(testProofFile.getFileType()).isEqualTo(UPDATED_FILE_TYPE);
        assertThat(testProofFile.getExtension()).isEqualTo(UPDATED_EXTENSION);
    }

    @Test
    public void updateNonExistingProofFile() throws Exception {
        int databaseSizeBeforeUpdate = proofFileRepository.findAll().size();

        // Create the ProofFile
        ProofFileDTO proofFileDTO = proofFileMapper.toDto(proofFile);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restProofFileMockMvc.perform(put("/api/proof-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(proofFileDTO)))
            .andExpect(status().isCreated());

        // Validate the ProofFile in the database
        List<ProofFile> proofFileList = proofFileRepository.findAll();
        assertThat(proofFileList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteProofFile() throws Exception {
        // Initialize the database
        proofFileRepository.save(proofFile);
        int databaseSizeBeforeDelete = proofFileRepository.findAll().size();

        // Get the proofFile
        restProofFileMockMvc.perform(delete("/api/proof-files/{id}", proofFile.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ProofFile> proofFileList = proofFileRepository.findAll();
        assertThat(proofFileList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProofFile.class);
        ProofFile proofFile1 = new ProofFile();
        proofFile1.setId("id1");
        ProofFile proofFile2 = new ProofFile();
        proofFile2.setId(proofFile1.getId());
        assertThat(proofFile1).isEqualTo(proofFile2);
        proofFile2.setId("id2");
        assertThat(proofFile1).isNotEqualTo(proofFile2);
        proofFile1.setId(null);
        assertThat(proofFile1).isNotEqualTo(proofFile2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProofFileDTO.class);
        ProofFileDTO proofFileDTO1 = new ProofFileDTO();
        proofFileDTO1.setId("id1");
        ProofFileDTO proofFileDTO2 = new ProofFileDTO();
        assertThat(proofFileDTO1).isNotEqualTo(proofFileDTO2);
        proofFileDTO2.setId(proofFileDTO1.getId());
        assertThat(proofFileDTO1).isEqualTo(proofFileDTO2);
        proofFileDTO2.setId("id2");
        assertThat(proofFileDTO1).isNotEqualTo(proofFileDTO2);
        proofFileDTO1.setId(null);
        assertThat(proofFileDTO1).isNotEqualTo(proofFileDTO2);
    }
}
