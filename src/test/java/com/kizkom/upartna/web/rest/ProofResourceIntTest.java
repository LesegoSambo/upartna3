package com.kizkom.upartna.web.rest;

import com.kizkom.upartna.UpartnaApp;

import com.kizkom.upartna.domain.Proof;
import com.kizkom.upartna.repository.ProofRepository;
import com.kizkom.upartna.service.ProofService;
import com.kizkom.upartna.service.dto.ProofDTO;
import com.kizkom.upartna.service.mapper.ProofMapper;
import com.kizkom.upartna.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.kizkom.upartna.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ProofResource REST controller.
 *
 * @see ProofResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = UpartnaApp.class)
public class ProofResourceIntTest {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_ON = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private ProofRepository proofRepository;

    @Autowired
    private ProofMapper proofMapper;

    @Autowired
    private ProofService proofService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restProofMockMvc;

    private Proof proof;

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Proof createEntity() {
        Proof proof = new Proof()
            .title(DEFAULT_TITLE)
            .description(DEFAULT_DESCRIPTION)
            .createdOn(DEFAULT_CREATED_ON);
        return proof;
    }

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProofResource proofResource = new ProofResource(proofService);
        this.restProofMockMvc = MockMvcBuilders.standaloneSetup(proofResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        proofRepository.deleteAll();
        proof = createEntity();
    }

    @Test
    public void createProof() throws Exception {
        int databaseSizeBeforeCreate = proofRepository.findAll().size();

        // Create the Proof
        ProofDTO proofDTO = proofMapper.toDto(proof);
        restProofMockMvc.perform(post("/api/proofs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(proofDTO)))
            .andExpect(status().isCreated());

        // Validate the Proof in the database
        List<Proof> proofList = proofRepository.findAll();
        assertThat(proofList).hasSize(databaseSizeBeforeCreate + 1);
        Proof testProof = proofList.get(proofList.size() - 1);
        assertThat(testProof.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testProof.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testProof.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
    }

    @Test
    public void createProofWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = proofRepository.findAll().size();

        // Create the Proof with an existing ID
        proof.setId("existing_id");
        ProofDTO proofDTO = proofMapper.toDto(proof);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProofMockMvc.perform(post("/api/proofs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(proofDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Proof in the database
        List<Proof> proofList = proofRepository.findAll();
        assertThat(proofList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllProofs() throws Exception {
        // Initialize the database
        proofRepository.save(proof);

        // Get all the proofList
        restProofMockMvc.perform(get("/api/proofs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(proof.getId())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())));
    }

    @Test
    public void getProof() throws Exception {
        // Initialize the database
        proofRepository.save(proof);

        // Get the proof
        restProofMockMvc.perform(get("/api/proofs/{id}", proof.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(proof.getId()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()));
    }

    @Test
    public void getNonExistingProof() throws Exception {
        // Get the proof
        restProofMockMvc.perform(get("/api/proofs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateProof() throws Exception {
        // Initialize the database
        proofRepository.save(proof);
        int databaseSizeBeforeUpdate = proofRepository.findAll().size();

        // Update the proof
        Proof updatedProof = proofRepository.findOne(proof.getId());
        updatedProof
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION)
            .createdOn(UPDATED_CREATED_ON);
        ProofDTO proofDTO = proofMapper.toDto(updatedProof);

        restProofMockMvc.perform(put("/api/proofs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(proofDTO)))
            .andExpect(status().isOk());

        // Validate the Proof in the database
        List<Proof> proofList = proofRepository.findAll();
        assertThat(proofList).hasSize(databaseSizeBeforeUpdate);
        Proof testProof = proofList.get(proofList.size() - 1);
        assertThat(testProof.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testProof.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testProof.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
    }

    @Test
    public void updateNonExistingProof() throws Exception {
        int databaseSizeBeforeUpdate = proofRepository.findAll().size();

        // Create the Proof
        ProofDTO proofDTO = proofMapper.toDto(proof);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restProofMockMvc.perform(put("/api/proofs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(proofDTO)))
            .andExpect(status().isCreated());

        // Validate the Proof in the database
        List<Proof> proofList = proofRepository.findAll();
        assertThat(proofList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteProof() throws Exception {
        // Initialize the database
        proofRepository.save(proof);
        int databaseSizeBeforeDelete = proofRepository.findAll().size();

        // Get the proof
        restProofMockMvc.perform(delete("/api/proofs/{id}", proof.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Proof> proofList = proofRepository.findAll();
        assertThat(proofList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Proof.class);
        Proof proof1 = new Proof();
        proof1.setId("id1");
        Proof proof2 = new Proof();
        proof2.setId(proof1.getId());
        assertThat(proof1).isEqualTo(proof2);
        proof2.setId("id2");
        assertThat(proof1).isNotEqualTo(proof2);
        proof1.setId(null);
        assertThat(proof1).isNotEqualTo(proof2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProofDTO.class);
        ProofDTO proofDTO1 = new ProofDTO();
        proofDTO1.setId("id1");
        ProofDTO proofDTO2 = new ProofDTO();
        assertThat(proofDTO1).isNotEqualTo(proofDTO2);
        proofDTO2.setId(proofDTO1.getId());
        assertThat(proofDTO1).isEqualTo(proofDTO2);
        proofDTO2.setId("id2");
        assertThat(proofDTO1).isNotEqualTo(proofDTO2);
        proofDTO1.setId(null);
        assertThat(proofDTO1).isNotEqualTo(proofDTO2);
    }
}
