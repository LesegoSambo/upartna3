package com.kizkom.upartna.web.rest;

import com.kizkom.upartna.UpartnaApp;

import com.kizkom.upartna.domain.RealizationFile;
import com.kizkom.upartna.repository.RealizationFileRepository;
import com.kizkom.upartna.service.RealizationFileService;
import com.kizkom.upartna.service.dto.RealizationFileDTO;
import com.kizkom.upartna.service.mapper.RealizationFileMapper;
import com.kizkom.upartna.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static com.kizkom.upartna.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RealizationFileResource REST controller.
 *
 * @see RealizationFileResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = UpartnaApp.class)
public class RealizationFileResourceIntTest {

    @Autowired
    private RealizationFileRepository realizationFileRepository;

    @Autowired
    private RealizationFileMapper realizationFileMapper;

    @Autowired
    private RealizationFileService realizationFileService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restRealizationFileMockMvc;

    private RealizationFile realizationFile;

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RealizationFile createEntity() {
        RealizationFile realizationFile = new RealizationFile();
        return realizationFile;
    }

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RealizationFileResource realizationFileResource = new RealizationFileResource(realizationFileService);
        this.restRealizationFileMockMvc = MockMvcBuilders.standaloneSetup(realizationFileResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        realizationFileRepository.deleteAll();
        realizationFile = createEntity();
    }

    @Test
    public void createRealizationFile() throws Exception {
        int databaseSizeBeforeCreate = realizationFileRepository.findAll().size();

        // Create the RealizationFile
        RealizationFileDTO realizationFileDTO = realizationFileMapper.toDto(realizationFile);
        restRealizationFileMockMvc.perform(post("/api/realization-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(realizationFileDTO)))
            .andExpect(status().isCreated());

        // Validate the RealizationFile in the database
        List<RealizationFile> realizationFileList = realizationFileRepository.findAll();
        assertThat(realizationFileList).hasSize(databaseSizeBeforeCreate + 1);
        RealizationFile testRealizationFile = realizationFileList.get(realizationFileList.size() - 1);
    }

    @Test
    public void createRealizationFileWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = realizationFileRepository.findAll().size();

        // Create the RealizationFile with an existing ID
        realizationFile.setId("existing_id");
        RealizationFileDTO realizationFileDTO = realizationFileMapper.toDto(realizationFile);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRealizationFileMockMvc.perform(post("/api/realization-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(realizationFileDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RealizationFile in the database
        List<RealizationFile> realizationFileList = realizationFileRepository.findAll();
        assertThat(realizationFileList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllRealizationFiles() throws Exception {
        // Initialize the database
        realizationFileRepository.save(realizationFile);

        // Get all the realizationFileList
        restRealizationFileMockMvc.perform(get("/api/realization-files?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(realizationFile.getId())));
    }

    @Test
    public void getRealizationFile() throws Exception {
        // Initialize the database
        realizationFileRepository.save(realizationFile);

        // Get the realizationFile
        restRealizationFileMockMvc.perform(get("/api/realization-files/{id}", realizationFile.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(realizationFile.getId()));
    }

    @Test
    public void getNonExistingRealizationFile() throws Exception {
        // Get the realizationFile
        restRealizationFileMockMvc.perform(get("/api/realization-files/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateRealizationFile() throws Exception {
        // Initialize the database
        realizationFileRepository.save(realizationFile);
        int databaseSizeBeforeUpdate = realizationFileRepository.findAll().size();

        // Update the realizationFile
        RealizationFile updatedRealizationFile = realizationFileRepository.findOne(realizationFile.getId());
        RealizationFileDTO realizationFileDTO = realizationFileMapper.toDto(updatedRealizationFile);

        restRealizationFileMockMvc.perform(put("/api/realization-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(realizationFileDTO)))
            .andExpect(status().isOk());

        // Validate the RealizationFile in the database
        List<RealizationFile> realizationFileList = realizationFileRepository.findAll();
        assertThat(realizationFileList).hasSize(databaseSizeBeforeUpdate);
        RealizationFile testRealizationFile = realizationFileList.get(realizationFileList.size() - 1);
    }

    @Test
    public void updateNonExistingRealizationFile() throws Exception {
        int databaseSizeBeforeUpdate = realizationFileRepository.findAll().size();

        // Create the RealizationFile
        RealizationFileDTO realizationFileDTO = realizationFileMapper.toDto(realizationFile);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restRealizationFileMockMvc.perform(put("/api/realization-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(realizationFileDTO)))
            .andExpect(status().isCreated());

        // Validate the RealizationFile in the database
        List<RealizationFile> realizationFileList = realizationFileRepository.findAll();
        assertThat(realizationFileList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteRealizationFile() throws Exception {
        // Initialize the database
        realizationFileRepository.save(realizationFile);
        int databaseSizeBeforeDelete = realizationFileRepository.findAll().size();

        // Get the realizationFile
        restRealizationFileMockMvc.perform(delete("/api/realization-files/{id}", realizationFile.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<RealizationFile> realizationFileList = realizationFileRepository.findAll();
        assertThat(realizationFileList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RealizationFile.class);
        RealizationFile realizationFile1 = new RealizationFile();
        realizationFile1.setId("id1");
        RealizationFile realizationFile2 = new RealizationFile();
        realizationFile2.setId(realizationFile1.getId());
        assertThat(realizationFile1).isEqualTo(realizationFile2);
        realizationFile2.setId("id2");
        assertThat(realizationFile1).isNotEqualTo(realizationFile2);
        realizationFile1.setId(null);
        assertThat(realizationFile1).isNotEqualTo(realizationFile2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RealizationFileDTO.class);
        RealizationFileDTO realizationFileDTO1 = new RealizationFileDTO();
        realizationFileDTO1.setId("id1");
        RealizationFileDTO realizationFileDTO2 = new RealizationFileDTO();
        assertThat(realizationFileDTO1).isNotEqualTo(realizationFileDTO2);
        realizationFileDTO2.setId(realizationFileDTO1.getId());
        assertThat(realizationFileDTO1).isEqualTo(realizationFileDTO2);
        realizationFileDTO2.setId("id2");
        assertThat(realizationFileDTO1).isNotEqualTo(realizationFileDTO2);
        realizationFileDTO1.setId(null);
        assertThat(realizationFileDTO1).isNotEqualTo(realizationFileDTO2);
    }
}
