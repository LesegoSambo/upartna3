'use strict';

describe('Controller Tests', function () {

    describe('RealizationFile Management Detail Controller', function () {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockRealizationFile;
        var createController;

        beforeEach(inject(function ($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockRealizationFile = jasmine.createSpy('MockRealizationFile');


            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'RealizationFile': MockRealizationFile
            };
            createController = function () {
                $injector.get('$controller')("RealizationFileDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function () {
            it('Unregisters root scope listener upon scope destruction', function () {
                var eventType = 'upartnaApp:realizationFileUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
